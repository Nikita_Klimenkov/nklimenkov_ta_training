package test.java.com.nklimenkov.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import test.java.com.nklimenkov.service.GmailService;

public class SignOutTest extends BaseTest {

	private GmailService gmailService = new GmailService();

	@Test
	public void testSignOut() {
		gmailService.signIn(account);
		gmailService.signOut();
		Assert.assertTrue(gmailService.signInPageIsDisplayed());
	}
}
