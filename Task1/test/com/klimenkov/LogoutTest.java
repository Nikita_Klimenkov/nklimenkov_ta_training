package com.klimenkov;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class LogoutTest extends BaseConfiguration {

	@Test
	public void testLogout() {

		MailService.login();
		MailService.logout();

		wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.PASSWORD_TEXTBOX_LOCATOR));
		Assert.assertTrue(driver.findElement(Locators.PASSWORD_TEXTBOX_LOCATOR).isDisplayed());
	}
}
