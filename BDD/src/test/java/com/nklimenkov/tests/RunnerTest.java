package test.java.com.nklimenkov.tests;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.testng.annotations.AfterMethod;
import test.java.com.nklimenkov.browser.Browser;

@CucumberOptions(
		features = "src/test/resources/features", 
		glue = "test.java.com.nklimenkov.stepdefs", 
		tags = "@searchProduct, @addToCart", 
		strict = true
		)

public class RunnerTest extends AbstractTestNGCucumberTests {
	@AfterMethod
	public static void cleanUp() {
		Browser.teardown();
	}
}
