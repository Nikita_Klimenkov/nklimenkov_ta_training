package test.java.com.nklimenkov.browser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ChromeWebDriver implements Driver {
	@Override
	public WebDriver getTargetDriver() {
		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
		return new ChromeDriver();
	}
}
