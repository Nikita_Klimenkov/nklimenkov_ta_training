package test.java.com.nklimenkov.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import test.java.com.nklimenkov.pages.DroppablePage;

public class DroppablePageTest extends BaseTest {
	@Test
	public void testDragAndDropElement() {
		DroppablePage droppablePage = mainPage.openDroppablePage();
		droppablePage.switchToDemoFrame();
		droppablePage.dragAndDropElement();
		Assert.assertEquals(droppablePage.getTargetElementText(), "Dropped!");
	}
}
