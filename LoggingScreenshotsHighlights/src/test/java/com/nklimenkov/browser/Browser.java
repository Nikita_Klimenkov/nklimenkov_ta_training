package test.java.com.nklimenkov.browser;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Browser {

	private static WebDriver driver;
	private static Logger logger = Logger.getLogger(Browser.class.getName());
	private static final int DEFAULT_TIMEOUT = 10;

	public Browser() {
		driver = getWebDriverInstance();
	}

	public static WebDriver getWebDriverInstance() {
		if (driver == null) {
			initializeWebDriver();
		}
		return driver;
	}

	private static void initializeWebDriver() {
		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(DEFAULT_TIMEOUT, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	public static void teardown() {
		if (driver != null) {
			driver.quit();
			driver = null;
		}
	}

	public void open(String URL) {
		driver.get(URL);
	}

	public String getTitle() {
		return driver.getTitle();
	}

	public String getCurrentUrl() {
		return driver.getCurrentUrl();
	}

	public boolean isPresent(By locator) {
		return !driver.findElements(locator).isEmpty();
	}

	public boolean isDisplayed(By locator) {
		return driver.findElement(locator).isDisplayed();
	}

	public boolean isEnabled(By locator) {
		return driver.findElement(locator).isEnabled();
	}

	public void waitForElementToBePresent(By locator) {
		new WebDriverWait(driver, DEFAULT_TIMEOUT).until(ExpectedConditions.presenceOfElementLocated(locator));
	}

	public void waitForElementToBeClickable(By locator) {
		new WebDriverWait(driver, DEFAULT_TIMEOUT).until(ExpectedConditions.elementToBeClickable(locator));
	}

	public void clear(By locator) {
		waitForElementToBeClickable(locator);
		driver.findElement(locator).clear();
	}

	public void click(By locator) {
		waitForElementToBeClickable(locator);
		driver.findElement(locator).click();
	}

	public void sendKeys(By locator, CharSequence keysToSend) {
		waitForElementToBeClickable(locator);
		driver.findElement(locator).sendKeys(keysToSend);
	}

	public String getText(By locator) {
		waitForElementToBeClickable(locator);
		return driver.findElement(locator).getText();
	}

	public String getAttribute(By locator, String attribute) {
		waitForElementToBePresent(locator);
		return driver.findElement(locator).getAttribute(attribute);
	}

	public void highlightElement(By locator, int px, String color) {
		((JavascriptExecutor) driver).executeScript("arguments[0].style.border='" + px + "px solid " + color + "'",
				driver.findElement(locator));
	}

	public void unHighlightElement(By locator) {
		((JavascriptExecutor) driver).executeScript("arguments[0].style.border='0px'", driver.findElement(locator));
	}

	public void takeScreenshot(String message) {
		try {
			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss-SSS");
			String fileName = ("Screenshot_") + message + "_" + dateFormat.format(new Date()) + ".png";
			File copy = new File(fileName);
			FileUtils.copyFile(screenshot, copy);
			FileUtils.moveFileToDirectory(copy, new File("screenshots/"), true);
			logger.info("Saved screenshot: screenshots/" + fileName);
		} catch (Exception e) {
			logger.error("Failed to make screenshot " + message);
		}
	}
}
