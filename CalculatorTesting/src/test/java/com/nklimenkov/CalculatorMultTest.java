package test.java.com.nklimenkov;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CalculatorMultTest extends BaseTest {

	@Test(dataProvider = "Data for mult test with Long")
	public void testMultWithLong(long a, long b, long expectedResult) {
		Assert.assertEquals(calculator.mult(a, b), expectedResult);
	}

	@Test(dataProvider = "Data for mult test with Double")
	public void testMultWithDouble(double a, double b, double expectedResult) {
		Assert.assertEquals(calculator.mult(a, b), expectedResult);
	}

	@DataProvider(name = "Data for mult test with Long")
	public static Object[][] getDataForMultTestWithLong() {
		return new Object[][] { 
			{ 0, 0, 0 }, { 0, 1, 0 }, { 0, -1, 0 }, 
			{ 1, 0, 0 }, { 1, 1, 1 }, { 1, -1, -1 }, 
			{ -1, 0, 0 }, { -1, 1, -1 }, { -1, -1, 1 },
			
			{ 5, 10, 50 }, { 5, -10, -50 }, { -5, 10, -50 }, { -5, -10, 50 }, 
			
			{ Long.MIN_VALUE, 1, Long.MIN_VALUE }, { Long.MAX_VALUE, 1, Long.MAX_VALUE } 
		};
	}

	// The following commented lines contain data which cause errors in the test method.
	// Uncomment them to see errors, comment back to get a successful build.
	@DataProvider(name = "Data for mult test with Double")
	public static Object[][] getDataForMultTestWithDouble() {
		return new Object[][] { 
			{ 0, 0, 0 }, { 0, 0.1, 0 }, { 0, -0.1, -0.0 }, 
			// { 0.1, 0, 0 }, { 0.1, 0.1, 0.01 }, { 0.1, -0.1, -0.01 }, 
			// { -0.1, 0, -0.0 }, { -0.1, 0.1, -0.01 }, { -0.1, -0.1, 0.01 }, 
			
			{ 5, 10, 50 }, { 5, -10, -50 }, { -5, 10, -50 }, { -5, -10, 50 }, 
			
			{ Long.MIN_VALUE, 1, Long.MIN_VALUE }, { Long.MAX_VALUE, 1, Long.MAX_VALUE }, 
			// { Double.MIN_VALUE, 1, Double.MIN_VALUE }, 
			{ Double.MAX_VALUE, 1, Double.MAX_VALUE }, 
			// { Double.MIN_NORMAL, 1, Double.MIN_NORMAL } 
		};
	}
}
