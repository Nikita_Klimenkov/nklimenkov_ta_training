package test.java.com.nklimenkov.browser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

public class EdgeWebDriver implements Driver {
	@Override
	public WebDriver getTargetDriver() {
		System.setProperty("webdriver.opera.driver", "src\\test\\resources\\drivers\\MicrosoftWebDriver.exe");
		return new EdgeDriver();
	}
}
