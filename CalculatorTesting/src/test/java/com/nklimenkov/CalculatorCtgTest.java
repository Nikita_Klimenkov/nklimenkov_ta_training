package test.java.com.nklimenkov;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CalculatorCtgTest extends BaseTest {

	// these test method is commented to get a successful build, uncomment it to see errors
//	@Test(dataProvider = "Data for ctg test")
//	public void testCtg(double a, double expectedResult) {
//		Assert.assertEquals(calculator.ctg(a), expectedResult, 0.00001);
//	}

	@DataProvider(name = "Data for ctg test")
	public static Object[][] getDataForTgTest() {
		return new Object[][] { 
			{ 0, Double.POSITIVE_INFINITY }, { 0.1, 9.966644423259238 }, { -0.1, -9.966644423259238 }, 
			{ 1, 0.6420926159343306 }, { -1, -0.6420926159343306 }, 
			
			{ Math.toRadians(30), 1.7320508075688783 }, { Math.toRadians(45), 1 }, 
			{ Math.toRadians(60), 0.5773502691896257 }, { Math.toRadians(90), 6.123233995736766E-17 }, 
			{ Math.toRadians(120), -0.5773502691896257 }, { Math.toRadians(180), -8.165619676597685E15 }, 
			{ Math.toRadians(270), 0 }, { Math.toRadians(360), -4.0828098382988425E15 }, 
			
			{ Long.MIN_VALUE, -0.011800898130584457 }, { Long.MAX_VALUE, 0.011800898130584457 }, 
			{ Double.MIN_VALUE, Double.POSITIVE_INFINITY }, { Double.MAX_VALUE, -201.53099572900314 }, 
			{ Double.MIN_NORMAL, 4.49423283715579E307 } 
		};
	}
}
