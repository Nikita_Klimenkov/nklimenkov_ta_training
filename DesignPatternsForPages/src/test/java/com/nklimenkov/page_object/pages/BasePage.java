package test.java.com.nklimenkov.page_object.pages;

import test.java.com.nklimenkov.page_object.browser.Browser;

public class BasePage {

	protected Browser browser;

	public BasePage() {
		browser = new Browser();
	}
}
