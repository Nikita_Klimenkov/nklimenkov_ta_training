package test.java.com.nklimenkov.page_factory.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GmailPage extends BasePage {

	@FindBy(xpath = "//a[@class='gb_b gb_fb gb_R']")
	private WebElement accountLink;

	@FindBy(id = "gb_71")
	private WebElement accountSingOutButton;

	@FindBy(xpath = "//div[@class='T-I J-J5-Ji T-I-KE L3']")
	private WebElement composeButton;

	@FindBy(xpath = "//div[@role='main']//table[@class='F cf zt']//tr[1]//div[@class='y6']")
	private WebElement latestMessageLink;

	private static final String SENT_MAIL_LINK_XPATH = "//div[@class='TN GLujEb aHS-bnu']//a";
	private static final String DRAFTS_LINK_XPATH = "//div[@class='TN GLujEb aHS-bnq']//a";
	private static final String HIGHLITED_MENU_LINK_ATTRIBUTE_XPATH = "[@tabindex='0']";

	public boolean isDisplayed() {
		browser.wait(accountLink);
		return browser.getTitle().contains("Gmail") && browser.getCurrentUrl().contains("https://mail.google.com/mail")
				&& browser.isPresentAndDisplayed(accountLink);
	}

	public GmailPage clickAccountLink() {
		browser.click(accountLink);
		return this;
	}

	public SignInPage clickAccountSignOutButton() {
		browser.click(accountSingOutButton);
		return new SignInPage();
	}

	public GmailWindowMessagePage clickComposeButton() {
		browser.click(composeButton);
		return new GmailWindowMessagePage();
	}

	public GmailSentMailPage openSentMail() {
		browser.click(browser.findElementByXpath(SENT_MAIL_LINK_XPATH));
		browser.wait(browser.findElementByXpath(SENT_MAIL_LINK_XPATH + HIGHLITED_MENU_LINK_ATTRIBUTE_XPATH));
		return new GmailSentMailPage();
	}

	public GmailDraftsPage openDrafts() {
		browser.click(browser.findElementByXpath(DRAFTS_LINK_XPATH));
		browser.wait(browser.findElementByXpath(DRAFTS_LINK_XPATH + HIGHLITED_MENU_LINK_ATTRIBUTE_XPATH));
		return new GmailDraftsPage();
	}

	protected void openLatestMessage() {
		browser.click(latestMessageLink);
	}
}
