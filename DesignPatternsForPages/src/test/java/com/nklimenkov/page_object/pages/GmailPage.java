package test.java.com.nklimenkov.page_object.pages;

import org.openqa.selenium.By;

public class GmailPage extends BasePage {

	private static final By ACCOUNT_LINK_LOCATOR = By.xpath("//a[@class='gb_b gb_fb gb_R']");
	private static final By ACCOUNT_SIGN_OUT_BUTTON_LOCATOR = By.id("gb_71");
	private static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//div[@class='T-I J-J5-Ji T-I-KE L3']");
	private static final By LATEST_MESSAGE_LINK_LOCATOR = By
			.xpath("//div[@role='main']//table[@class='F cf zt']//tr[1]//div[@class='y6']");

	private static final String SENT_MAIL_LINK_XPATH = "//div[@class='TN GLujEb aHS-bnu']//a";
	private static final String DRAFTS_LINK_XPATH = "//div[@class='TN GLujEb aHS-bnq']//a";
	private static final String HIGHLITED_MENU_LINK_ATTRIBUTE_XPATH = "[@tabindex='0']";

	public boolean isDisplayed() {
		browser.wait(ACCOUNT_LINK_LOCATOR);
		return browser.getTitle().contains("Gmail") && browser.getCurrentUrl().contains("https://mail.google.com/mail")
				&& browser.isDisplayed(ACCOUNT_LINK_LOCATOR);
	}

	public GmailPage clickAccountLink() {
		browser.click(ACCOUNT_LINK_LOCATOR);
		return this;
	}

	public SignInPage clickAccountSignOutButton() {
		browser.click(ACCOUNT_SIGN_OUT_BUTTON_LOCATOR);
		return new SignInPage();
	}

	public GmailWindowMessagePage clickComposeButton() {
		browser.click(COMPOSE_BUTTON_LOCATOR);
		return new GmailWindowMessagePage();
	}

	public GmailSentMailPage openSentMail() {
		browser.click(By.xpath(SENT_MAIL_LINK_XPATH));
		browser.wait(By.xpath(SENT_MAIL_LINK_XPATH + HIGHLITED_MENU_LINK_ATTRIBUTE_XPATH));
		return new GmailSentMailPage();
	}

	public GmailDraftsPage openDrafts() {
		browser.click(By.xpath(DRAFTS_LINK_XPATH));
		browser.wait(By.xpath(DRAFTS_LINK_XPATH + HIGHLITED_MENU_LINK_ATTRIBUTE_XPATH));
		return new GmailDraftsPage();
	}

	protected void openLatestMessage() {
		browser.click(LATEST_MESSAGE_LINK_LOCATOR);
	}
}
