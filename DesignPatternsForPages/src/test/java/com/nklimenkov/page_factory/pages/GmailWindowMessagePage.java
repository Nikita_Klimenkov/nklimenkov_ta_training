package test.java.com.nklimenkov.page_factory.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GmailWindowMessagePage extends GmailPage {

	@FindBy(name = "to")
	private WebElement addresseeTextbox;

	@FindBy(xpath = "//div[@class='oL aDm az9']/span")
	private WebElement addresseeDraftTextbox;

	@FindBy(name = "subjectbox")
	private WebElement subjectTextbox;

	@FindBy(xpath = "//table[@class='aoP aoC']//input[@name='subject']")
	private WebElement subjectDraftTextbox;

	@FindBy(xpath = "//div[@class='Am Al editable LW-avf']")
	private WebElement contentTextbox;

	@FindBy(xpath = "//img[@class='Ha']")
	private WebElement closeButton;

	@FindBy(xpath = "//div[@class='T-I J-J5-Ji aoO T-I-atl L3']")
	private WebElement sendButton;

	public GmailWindowMessagePage typeAddressee(String addressee) {
		browser.sendKeys(addresseeTextbox, addressee);
		return this;
	}

	public String getAddressee() {
		browser.wait(addresseeDraftTextbox);
		return browser.getAttribute(addresseeDraftTextbox, "email");
	}

	public GmailWindowMessagePage typeSubject(String subject) {
		browser.sendKeys(subjectTextbox, subject);
		return this;
	}

	public String getSubject() {
		return browser.getAttribute(subjectDraftTextbox, "value");
	}

	public GmailWindowMessagePage typeContent(String content) {
		browser.sendKeys(contentTextbox, content);
		return this;
	}

	public String getContent() {
		return browser.getText(contentTextbox);
	}

	public GmailPage close() {
		browser.click(closeButton);
		return new GmailPage();
	}

	public GmailPage send() {
		browser.click(sendButton);
		return new GmailPage();
	}
}
