package test.java.com.nklimenkov.browser;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class Browser {

	private static WebDriver driver;
	private static final int DEFAULT_TIMEOUT = 10;

	public Browser() {
		driver = getWebDriverInstance();
	}

	public static WebDriver getWebDriverInstance() {
		if (driver == null) {
			initializeWebDriver();
		}
		return driver;
	}

	private static void initializeWebDriver() {
		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(DEFAULT_TIMEOUT, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	public static void teardown() {
		if (driver != null) {
			driver.quit();
			driver = null;
		}
	}

	public void open(String URL) {
		driver.get(URL);
	}

	public Object executeJS(String script) {
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
		return jsExecutor.executeScript(script);
	}

	public void switchToFrame(By locator) {
		driver.switchTo().frame(driver.findElement(locator));
	}

	public boolean isPresent(By locator) {
		return !driver.findElements(locator).isEmpty();
	}

	public boolean isSelected(By locator) {
		return driver.findElement(locator).isSelected();
	}

	public void wait(By locator) {
		new WebDriverWait(driver, DEFAULT_TIMEOUT).until(ExpectedConditions.elementToBeClickable(locator));
	}

	public void click(By locator) {
		wait(locator);
		driver.findElement(locator).click();
	}

	public String getText(By locator) {
		wait(locator);
		return driver.findElement(locator).getText();
	}

	public String getAttribute(By locator, String attribute) {
		return driver.findElement(locator).getAttribute(attribute);
	}

	public void moveToElement(By locator) {
		WebElement element = driver.findElement(locator);
		new Actions(driver).moveToElement(element).build().perform();

	}

	public void dragAndDrop(By sourceLocator, By targetLocator) {
		wait(sourceLocator);
		WebElement draggable = driver.findElement(sourceLocator);
		wait(targetLocator);
		WebElement droppable = driver.findElement(targetLocator);
		new Actions(driver).dragAndDrop(draggable, droppable).build().perform();
	}

	public void dragAndDropBy(By sourceLocator, int xOffset, int yOffset) {
		wait(sourceLocator);
		WebElement draggable = driver.findElement(sourceLocator);
		new Actions(driver).dragAndDropBy(draggable, xOffset, yOffset).build().perform();
	}

	public void dragAndDropBy(By sourceLocator, By targetLocator, int xOffset, int yOffset) {
		wait(sourceLocator);
		WebElement draggable = driver.findElement(sourceLocator);
		wait(targetLocator);
		WebElement droppable = driver.findElement(targetLocator);
		new Actions(driver).moveToElement(draggable).clickAndHold(draggable).moveToElement(droppable, xOffset, yOffset)
				.release().build().perform();
	}

	public void CtrlKeyDown() {
		new Actions(driver).keyDown(Keys.CONTROL).build().perform();
	}

	public void CtrlKeyUp() {
		new Actions(driver).keyUp(Keys.CONTROL).build().perform();
	}
}
