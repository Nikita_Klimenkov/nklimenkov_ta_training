package com.nklimenkov;

import static com.jayway.restassured.RestAssured.given;
import org.hamcrest.Matchers;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;

public class WebServicesTest {
	@BeforeTest
	public void setUp() {
		RestAssured.baseURI = "http://jsonplaceholder.typicode.com";
	}

	@Test
	public void verifyStatusCode() {
		given().
				when().get("/posts").
				then().statusCode(200);
	}

	@Test
	public void verifyResponseHeader() {
		given().
				when().get("/posts").
				then().header("content-type", "application/json; charset=utf-8");
	}

	@Test
	public void verifyResponseBody() {
		Response response = given().when().get("/posts");
		User[] users = response.as(User[].class);
		Assert.assertEquals(users.length, 100);
	}

	@Test
	public void createPost() {
		int id = given().
				body(new User()).
				contentType(ContentType.JSON).
				when().post("/posts").
				then().assertThat().statusCode(201).body("isEmpty()", Matchers.is(false)).
				extract().path("id");
		System.out.println(id);
	}

	@Test
	public void updatePost() {
		given().
				body(new User()).
				contentType(ContentType.JSON).
				when().put("/posts/1").
				then().assertThat().statusCode(200).body("id", Matchers.is(1));
	}

	@Test
	public void deletePost() {
		given().
				when().delete("/posts/1").
				then().assertThat().statusCode(200);
	}
}
