package main.java;

import java.util.Scanner;
import java.sql.ResultSet;
import java.sql.SQLException;

public class StaffTableUI {

    private static Scanner in;

    public static void startUserDialogue() {
        in = new Scanner(System.in);
        printInfo();
        String userInput = in.next();
        while (!userInput.equals("exit")) {
            switch (userInput) {
                case "info":
                    printInfo();
                    break;
                case "print":
                    printStaffTable();
                    break;
                case "add":
                    addNewEntry();
                    break;
                case "remove":
                    removeEntryById();
                    break;
                default:
                    System.out.println("The entered command is invalid. Try again.");
                    break;
            }
            System.out.println("\nEnter the command('info' for full description): ");
            userInput = in.next();
        }
        in.close();
    }

    private static void printInfo() {
        System.out.println("This console application emulates the work with the 'staff' table.");
        System.out.println("Available commands(enter without quotes):");
        System.out.println("'info' - prints this description of application and commands;");
        System.out.println("'print' - prints the 'staff' table;");
        System.out.println("'add' - adds new employee entry;");
        System.out.println("'remove' - removes employee entry by id;");
        System.out.println("'exit' - exits the program.");
    }

    private static void printStaffTable() {
        try {
            System.out.println("id name surname");
            ResultSet resultSet = StaffTable.getResultSet();
            while (resultSet.next()) {
                String id = resultSet.getString(1);
                String name = resultSet.getString(2);
                String surname = resultSet.getString(3);
                System.out.println(id + " " + name + " " + surname);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    private static void addNewEntry() {
        System.out.println("Adding a new employee entry...");
        int id = getIdToAddEntry();
        System.out.print("name: ");
        String name = in.next();
        System.out.print("surname: ");
        String surname = in.next();
        StaffTable.addEmployee(id, name, surname);
        System.out.println("New employee entry " + id + " " + name + " " + surname + " successfully added.");
    }

    private static void removeEntryById() {
        System.out.println("Removing employee entry...");
        int id = getIdToRemoveEntry();
        StaffTable.removeEmployeeWithId(id);
        System.out.println("Employee entry with id " + id + " successfully removed.");
    }

    private static int getIdToAddEntry() {
        int id;
        do {
            id = getId("Enter new id(must be a unique integer number >= 0): ");
        } while (StaffTable.containsEmployeeWithId(id));
        return id;
    }

    private static int getIdToRemoveEntry() {
        int id;
        do {
            id = getId("Enter existing employee entry id that you want to remove: ");
        } while (!StaffTable.containsEmployeeWithId(id));
        return id;
    }

    private static int getId(String message) {
        System.out.print(message);
        String stringId = in.next();
        while (!stringId.matches("[0-9]+")) {
            System.out.println("id must contain only digits 0-9.");
            System.out.print(message);
            stringId = in.next();
        }
        return Integer.parseInt(stringId);
    }
}
