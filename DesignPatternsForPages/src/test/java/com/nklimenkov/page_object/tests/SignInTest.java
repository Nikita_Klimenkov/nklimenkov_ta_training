package test.java.com.nklimenkov.page_object.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import test.java.com.nklimenkov.bo.Account;

public class SignInTest extends BaseTest {
	@Test
	public void testSignIn() {
		gmailService.signIn(Account.getDefaultAccount());
		Assert.assertTrue(gmailService.gmailPageIsDisplayed());
		gmailService.signOut();
	}
}
