package test.java.com.nklimenkov.page_object.tests;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import test.java.com.nklimenkov.bo.Account;
import test.java.com.nklimenkov.bo.Message;

public class SendTest extends BaseTest {
	@Test
	public void testSendNewMessage() {
		gmailService.signIn(Account.getDefaultAccount());
		gmailService.sendNewMessage(Message.getDefaultMessage());
		Message sentMessage = gmailService.getLatestSentMessage();
		Assert.assertTrue(sentMessage.equals(Message.getDefaultMessage()));
	}

	@Test
	public void testCreateAndSendNewDraftMessage() {
		gmailService.signIn(Account.getDefaultAccount());
		gmailService.createNewDraftMessage(Message.getDefaultMessage());
		gmailService.sendLatestDraftMessage();
		Message sentMessage = gmailService.getLatestSentMessage();
		Assert.assertTrue(sentMessage.equals(Message.getDefaultMessage()));
	}

	@AfterMethod
	public void signOut() {
		gmailService.signOut();
	}
}
