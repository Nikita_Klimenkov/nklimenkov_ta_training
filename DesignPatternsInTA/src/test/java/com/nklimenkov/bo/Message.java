package test.java.com.nklimenkov.bo;

public class Message {

	private String addressee;
	private String subject;
	private String content;

	public Message(String addressee, String subject, String content) {
		this.addressee = addressee;
		this.subject = subject;
		this.content = content;
	}

	public String getAddressee() {
		return addressee;
	}

	public String getSubject() {
		return subject;
	}

	public String getContent() {
		return content;
	}

	public boolean equals(Message message) {
		return this.addressee.equals(message.addressee) && this.subject.equals(message.subject)
				&& this.content.equals(message.content);
	}
}
