package test.java.com.nklimenkov;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CalculatorDivTest extends BaseTest {

	@Test(dataProvider = "Data for div test with Long")
	public void testDivWithLong(long a, long b, long expectedResult) {
		Assert.assertEquals(calculator.div(a, b), expectedResult);
	}

	@Test(dependsOnMethods = "testDivWithLong", alwaysRun = true, expectedExceptions = NumberFormatException.class, 
			dataProvider = "Data for div test with Long expecting NumberFormatException")
	public void testDivWithLongExpectingNumberFormatException(long a, long b) {
		calculator.div(a, b);
	}

	@Test(dataProvider = "Data for div test with Double")
	public void testDivWithDouble(double a, double b, double expectedResult) {
		Assert.assertEquals(calculator.div(a, b), expectedResult);
	}

	@DataProvider(name = "Data for div test with Long")
	public static Object[][] getDataForDivTestWithLong() {
		return new Object[][] { 
			{ 0, 1, 0 }, { 0, -1, 0 },  
			{ 1, 1, 1 }, { 1, -1, -1 }, { -1, 1, -1 }, { -1, -1, 1 }, 
			
			{ 5, 10, 0 }, { 5, -10, 0 }, { -5, 10, 0 }, { -5, -10, 0 }, 
			{ 10, 5, 2 }, { 10, -5, -2 }, { -10, 5, -2 }, { -10, -5, 2 },
			
			{ Long.MIN_VALUE, 1, Long.MIN_VALUE }, { Long.MAX_VALUE, 1, Long.MAX_VALUE } 
		};
	}

	@DataProvider(name = "Data for div test with Long expecting NumberFormatException")
	public static Object[][] getDataForDivTestWithLongExpectingNumberFormatException() {
		return new Object[][] { 
			{ 0, 0 }, { 1, 0 }, { -1, 0 }, { 10, 0 }, { -10, 0 }, 
			{ Long.MIN_VALUE, 0}, { Long.MAX_VALUE, 0} 
		};
	}

	@DataProvider(name = "Data for div test with Double")
	public static Object[][] getDataForDivTestWithDouble() {
		return new Object[][] { 
			{ 0, 0, Double.NaN }, { 0, 0.1, 0 }, { 0, -0.1, -0.0 }, 
			
			{ 0.1, 0, Double.POSITIVE_INFINITY }, { 0.1, 0.1, 1 }, { 0.1, -0.1, -1 }, 
			{ -0.1, 0, Double.NEGATIVE_INFINITY  }, { -0.1, 0.1, -1 }, { -0.1, -0.1, 1 }, 
			
			{ 0, 10, 0}, { 0, -10, -0.0}, { 10, 0, Double.POSITIVE_INFINITY }, { -10, 0, Double.NEGATIVE_INFINITY }, 
			
			{ 5, 10, 0.5 }, { 5, -10, -0.5 }, { -5, 10, -0.5 }, { -5, -10, 0.5 },
			{ 10, 5, 2 }, { 10, -5, -2 }, { -10, 5, -2 }, { -10, -5, 2 },
			
			{ Long.MIN_VALUE, 1, Long.MIN_VALUE }, { Long.MAX_VALUE, 1, Long.MAX_VALUE }, 
			{ Double.MIN_VALUE, 1, Double.MIN_VALUE }, { Double.MAX_VALUE, 1, Double.MAX_VALUE }, 
			{ Double.MIN_NORMAL, 1, Double.MIN_NORMAL } 
		};
	}
}
