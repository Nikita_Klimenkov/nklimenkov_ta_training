package test.java.com.nklimenkov.bo;

public class AccountFactory {

	private static final String IDENTIFIER = "NikitaKlimenkovHere@gmail.com";
	private static final String PASSWORD = "supersecurepassword";

	public static Account getDefaultAccount() {
		return new Account(IDENTIFIER, PASSWORD);
	}
}
