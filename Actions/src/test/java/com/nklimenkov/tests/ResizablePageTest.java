package test.java.com.nklimenkov.tests;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import test.java.com.nklimenkov.pages.ResizablePage;

public class ResizablePageTest extends BaseTest {

	private ResizablePage resizablePage;
	private int width;
	private int height;
	private int xOffset = 100; // px
	private int yOffset = 100; // px

	@BeforeMethod
	public void setUpResizablePageTest() {
		resizablePage = mainPage.openResizablePage();
		resizablePage.switchToDemoFrame();
		width = resizablePage.getResizableElementWidth();
		height = resizablePage.getResizableElementHeight();
	}

	@Test
	public void testResizeElementByRightHandle() {
		resizablePage.resizeElementByRightHandle(xOffset);
		Assert.assertEquals(resizablePage.getResizableElementWidth(), width + xOffset);
		Assert.assertEquals(resizablePage.getResizableElementHeight(), height);
	}

	@Test
	public void testResizeElementByBottomHandle() {
		resizablePage.resizeElementByBottomHandle(yOffset);
		Assert.assertEquals(resizablePage.getResizableElementWidth(), width);
		Assert.assertEquals(resizablePage.getResizableElementHeight(), height + yOffset);
	}

	@Test
	public void testResizeElementByRightBottomHandle() {
		resizablePage.resizeElementByRightBottomHandle(xOffset, yOffset);
		Assert.assertEquals(resizablePage.getResizableElementWidth(), width + xOffset);
		Assert.assertEquals(resizablePage.getResizableElementHeight(), height + yOffset);
	}
}
