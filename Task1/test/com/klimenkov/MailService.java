package com.klimenkov;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class MailService extends BaseConfiguration {

	private static String email = "NikitaKlimenkovHere@gmail.com";
	private static String password = "seleniumtest";

	private static String addressee = "Mikita_Klimiankou@epam.com";
	private static String subject = "Test";
	private static String content = "Selenium WebDriver Basics Task 1 Test";

	public static String getAddressee() {
		return addressee;
	}

	public static String getSubject() {
		return subject;
	}

	public static String getContent() {
		return content;
	}

	public static void login() {

		wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.IDENTIFIER_TEXTBOX_LOCATOR));
		driver.findElement(Locators.IDENTIFIER_TEXTBOX_LOCATOR).sendKeys(email + Keys.ENTER);

		wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.PASSWORD_TEXTBOX_LOCATOR));
		driver.findElement(Locators.PASSWORD_TEXTBOX_LOCATOR).sendKeys(password + Keys.ENTER);
	}

	public static void logout() {

		wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.ACCOUNT_ICON_LOCATOR));
		driver.findElement(Locators.ACCOUNT_ICON_LOCATOR).click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.ACCOUNT_EXIT_BUTTON_LOCATOR));
		driver.findElement(Locators.ACCOUNT_EXIT_BUTTON_LOCATOR).click();
	}

	public static void createDraft() {

		wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.COMPOSE_BUTTON_LOCATOR));
		driver.findElement(Locators.COMPOSE_BUTTON_LOCATOR).click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.ADDRESSEE_TEXTBOX_LOCATOR));
		driver.findElement(Locators.ADDRESSEE_TEXTBOX_LOCATOR).sendKeys(addressee);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.SUBJECT_TEXTBOX_LOCATOR));
		driver.findElement(Locators.SUBJECT_TEXTBOX_LOCATOR).sendKeys(subject);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.CONTENT_TEXTBOX_LOCATOR));
		driver.findElement(Locators.CONTENT_TEXTBOX_LOCATOR).sendKeys(content);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.CLOSE_BUTTON_LOCATOR));
		driver.findElement(Locators.CLOSE_BUTTON_LOCATOR).click();
	}

	public static void openCreatedDraft() {

		wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.DRAFTS_BUTTON_LOCATOR));
		driver.findElement(Locators.DRAFTS_BUTTON_LOCATOR).click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.HIGHLITED_DRAFTS_BUTTON_LOCATOR));
		wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.LATEST_LETTER_LINK_LOCATOR));
		driver.findElement(Locators.LATEST_LETTER_LINK_LOCATOR).click();
	}

	public static void sendOpenedDraft() {

		wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.SEND_BUTTON_LOCATOR));
		driver.findElement(Locators.SEND_BUTTON_LOCATOR).click();
	}

	public static void openLastSentLetter() {

		wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.SENT_MAIL_BUTTON_LOCATOR));
		driver.findElement(Locators.SENT_MAIL_BUTTON_LOCATOR).click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.HIGHLITED_SENT_MAIL_BUTTON_LOCATOR));
		wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.LATEST_LETTER_LINK_LOCATOR));
		driver.findElement(Locators.LATEST_LETTER_LINK_LOCATOR).click();
	}

	public static boolean letterVerificationWithConversationViewOn() {

		// mail addressee check
		wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.ADDRESSEE_SPAN_LOCATOR));
		String draftAddressee = driver.findElement(Locators.ADDRESSEE_SPAN_LOCATOR).getAttribute("email");
		boolean addresseeIsConfirmed = draftAddressee.equals(MailService.getAddressee());

		// mail subject check
		wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.SUBJECT_HEADER_LOCATOR));
		String draftSubject = driver.findElement(Locators.SUBJECT_HEADER_LOCATOR).getText();
		boolean subjectIsConfirmed = draftSubject.equals(MailService.getSubject());

		// mail content check
		wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.CONTENT_DIV_CONVERSATION_VIEW_ON_LOCATOR));
		String draftContent = driver.findElement(Locators.CONTENT_DIV_CONVERSATION_VIEW_ON_LOCATOR).getText();
		boolean contentIsConfirmed = draftContent.equals(MailService.getContent());

		return addresseeIsConfirmed && subjectIsConfirmed && contentIsConfirmed;
	}

	public static boolean draftLetterVerificationWithConversationViewOff() {

		// mail addressee check
		wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.ADDRESSEE_DRAFT_TEXTBOX_LOCATOR));
		String draftAddressee = driver.findElement(Locators.ADDRESSEE_DRAFT_TEXTBOX_LOCATOR).getAttribute("email");
		boolean addresseeIsConfirmed = draftAddressee.equals(MailService.getAddressee());

		// mail subject check
		wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.SUBJECT_TEXTBOX_LOCATOR));
		String draftSubject = driver.findElement(Locators.SUBJECT_DRAFT_TEXTBOX_LOCATOR).getAttribute("value");
		boolean subjectIsConfirmed = draftSubject.equals(MailService.getSubject());

		// mail content check
		wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.CONTENT_TEXTBOX_LOCATOR));
		String draftContent = driver.findElement(Locators.CONTENT_TEXTBOX_LOCATOR).getText();
		boolean contentIsConfirmed = draftContent.equals(MailService.getContent());

		return addresseeIsConfirmed && subjectIsConfirmed && contentIsConfirmed;
	}

	public static boolean sentLetterVerificationWithConversationViewOff() {

		// mail addressee check
		wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.ADDRESSEE_SPAN_LOCATOR));
		String draftAddressee = driver.findElement(Locators.ADDRESSEE_SPAN_LOCATOR).getAttribute("email");
		boolean addresseeIsConfirmed = draftAddressee.equals(MailService.getAddressee());

		// mail subject check
		wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.SUBJECT_HEADER_LOCATOR));
		String draftSubject = driver.findElement(Locators.SUBJECT_HEADER_LOCATOR).getText();
		boolean subjectIsConfirmed = draftSubject.equals(MailService.getSubject());

		// mail content check
		wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.CONTENT_DIV_CONVERSATION_VIEW_OFF_LOCATOR));
		String draftContent = driver.findElement(Locators.CONTENT_DIV_CONVERSATION_VIEW_OFF_LOCATOR).getText();
		boolean contentIsConfirmed = draftContent.equals(MailService.getContent());

		return addresseeIsConfirmed && subjectIsConfirmed && contentIsConfirmed;
	}
}
