package test.java.com.nklimenkov.page_factory.pages;

import org.openqa.selenium.support.PageFactory;
import test.java.com.nklimenkov.page_factory.browser.Browser;

public class BasePage {

	protected Browser browser;

	public BasePage() {
		browser = new Browser();
		PageFactory.initElements(Browser.getWebDriverInstance(), this);
	}
}
