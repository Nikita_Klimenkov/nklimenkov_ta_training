package test.java.com.nklimenkov.tests;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import test.java.com.nklimenkov.pages.SelectMenuPage;

public class SelectMenuPageTest extends BaseTest {

	private SelectMenuPage selectMenuPage;

	@BeforeMethod
	public void setUpResizablePageTest() {
		selectMenuPage = mainPage.openSelectMenuPage();
		selectMenuPage.switchToDemoFrame();
	}

	@Test
	public void testSelectSpeed() {
		selectMenuPage.selectSpeed(1);
		Assert.assertEquals(selectMenuPage.getSelectedSpeed(), 1);

		selectMenuPage.selectSpeed(3);
		Assert.assertEquals(selectMenuPage.getSelectedSpeed(), 3);

		selectMenuPage.selectSpeed(5);
		Assert.assertEquals(selectMenuPage.getSelectedSpeed(), 5);
	}

	@Test
	public void testSelectFile() {
		selectMenuPage.selectFile(1);
		Assert.assertEquals(selectMenuPage.getSelectedFile(), 1);

		selectMenuPage.selectFile(2);
		Assert.assertEquals(selectMenuPage.getSelectedFile(), 2);

		selectMenuPage.selectFile(4);
		Assert.assertEquals(selectMenuPage.getSelectedFile(), 4);
	}

	@Test
	public void testSelectNumber() {
		selectMenuPage.selectNumber(1);
		Assert.assertEquals(selectMenuPage.getSelectedNumber(), 1);

		selectMenuPage.selectNumber(5);
		Assert.assertEquals(selectMenuPage.getSelectedNumber(), 5);

		selectMenuPage.selectNumber(9);
		Assert.assertEquals(selectMenuPage.getSelectedNumber(), 9);

		selectMenuPage.selectNumber(10);
		Assert.assertEquals(selectMenuPage.getSelectedNumber(), 10);

		selectMenuPage.selectNumber(19);
		Assert.assertEquals(selectMenuPage.getSelectedNumber(), 19);
	}

	@Test
	public void testSelectTitle() {
		selectMenuPage.selectTitle("Mr.");
		Assert.assertEquals(selectMenuPage.getSelectedTitle(), "Mr.");

		selectMenuPage.selectTitle("Mrs.");
		Assert.assertEquals(selectMenuPage.getSelectedTitle(), "Mrs.");

		selectMenuPage.selectTitle("Other");
		Assert.assertEquals(selectMenuPage.getSelectedTitle(), "Other");
	}
}
