package test.java.com.nklimenkov.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import test.java.com.nklimenkov.pages.SortablePage;

public class SortablePageTest extends BaseTest {
	@Test
	public void testMoveItemFromTo() {
		SortablePage sortablePage = mainPage.openSortablePage();
		sortablePage.switchToDemoFrame();

		sortablePage.moveItemFromTo(1, 1);
		int[] expectedOrderOfItems = { 1, 2, 3, 4, 5, 6, 7 };
		Assert.assertEquals(sortablePage.getOrderOfItems(), expectedOrderOfItems);

		sortablePage.moveItemFromTo(1, 2);
		expectedOrderOfItems = new int[] { 2, 1, 3, 4, 5, 6, 7 };
		Assert.assertEquals(sortablePage.getOrderOfItems(), expectedOrderOfItems);

		sortablePage.moveItemFromTo(2, 1);
		expectedOrderOfItems = new int[] { 1, 2, 3, 4, 5, 6, 7 };
		Assert.assertEquals(sortablePage.getOrderOfItems(), expectedOrderOfItems);

		sortablePage.moveItemFromTo(1, 7);
		expectedOrderOfItems = new int[] { 2, 3, 4, 5, 6, 7, 1 };
		Assert.assertEquals(sortablePage.getOrderOfItems(), expectedOrderOfItems);

		sortablePage.moveItemFromTo(3, 5);
		expectedOrderOfItems = new int[] { 2, 3, 5, 6, 4, 7, 1 };
		Assert.assertEquals(sortablePage.getOrderOfItems(), expectedOrderOfItems);

		sortablePage.moveItemFromTo(6, 4);
		expectedOrderOfItems = new int[] { 2, 3, 5, 7, 6, 4, 1 };
		expectedOrderOfItems = sortablePage.getOrderOfItems();
		Assert.assertEquals(sortablePage.getOrderOfItems(), expectedOrderOfItems);

		sortablePage.moveItemFromTo(7, 1);
		expectedOrderOfItems = sortablePage.getOrderOfItems();
		expectedOrderOfItems = new int[] { 1, 2, 3, 5, 7, 6, 4 };
		Assert.assertEquals(sortablePage.getOrderOfItems(), expectedOrderOfItems);
	}
}
