package test.java.com.nklimenkov;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CalculatorSinTest extends BaseTest {

	@Test(dataProvider = "Data for sin test")
	public void testSin(double a, double expectedResult) {
		Assert.assertEquals(calculator.sin(a), expectedResult, 0.00001);
	}

	@DataProvider(name = "Data for sin test")
	public static Object[][] getDataForSinTest() {
		return new Object[][] { 
			{ 0, 0 }, { 0.1, 0.09983341664682815 }, { -0.1, -0.09983341664682815 }, 
			{ 1, 0.8414709848078965 }, { -1, -0.8414709848078965 }, 
			
			{ Math.toRadians(30), 0.5 }, { Math.toRadians(45), 0.7071067811865475 }, 
			{ Math.toRadians(60), 0.8660254037844386 }, { Math.toRadians(90), 1 }, 
			{ Math.toRadians(120), 0.8660254037844386 }, { Math.toRadians(180), 0 }, 
			{ Math.toRadians(270), -1 }, { Math.toRadians(360), 0 }, 
			
			{ Long.MIN_VALUE, -0.9999303766734422 }, { Long.MAX_VALUE, 0.9999303766734422 }, 
			{ Double.MIN_VALUE, 4.9E-324 }, { Double.MAX_VALUE, 0.004961954789184062 }, 
			{ Double.MIN_NORMAL, 2.2250738585072014E-308 } 
		};
	}
}
