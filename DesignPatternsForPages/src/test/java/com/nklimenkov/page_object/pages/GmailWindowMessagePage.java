package test.java.com.nklimenkov.page_object.pages;

import org.openqa.selenium.By;

public class GmailWindowMessagePage extends GmailPage {

	private static final By ADDRESSEE_TEXTBOX_LOCATOR = By.name("to");
	private static final By ADDRESSEE_DRAFT_TEXTBOX_LOCATOR = By.xpath("//div[@class='oL aDm az9']/span");
	private static final By SUBJECT_TEXTBOX_LOCATOR = By.name("subjectbox");
	private static final By SUBJECT_DRAFT_TEXTBOX_LOCATOR = By
			.xpath("//table[@class='aoP aoC']//input[@name='subject']");
	private static final By CONTENT_TEXTBOX_LOCATOR = By.xpath("//div[@class='Am Al editable LW-avf']");
	private static final By CLOSE_BUTTON_LOCATOR = By.xpath("//img[@class='Ha']");
	private static final By SEND_BUTTON_LOCATOR = By.xpath("//div[@class='T-I J-J5-Ji aoO T-I-atl L3']");

	public GmailWindowMessagePage typeAddressee(String addressee) {
		browser.sendKeys(ADDRESSEE_TEXTBOX_LOCATOR, addressee);
		return this;
	}

	public String getAddressee() {
		browser.wait(ADDRESSEE_DRAFT_TEXTBOX_LOCATOR);
		return browser.getAttribute(ADDRESSEE_DRAFT_TEXTBOX_LOCATOR, "email");
	}

	public GmailWindowMessagePage typeSubject(String subject) {
		browser.sendKeys(SUBJECT_TEXTBOX_LOCATOR, subject);
		return this;
	}

	public String getSubject() {
		return browser.getAttribute(SUBJECT_DRAFT_TEXTBOX_LOCATOR, "value");
	}

	public GmailWindowMessagePage typeContent(String content) {
		browser.sendKeys(CONTENT_TEXTBOX_LOCATOR, content);
		return this;
	}

	public String getContent() {
		return browser.getText(CONTENT_TEXTBOX_LOCATOR);
	}

	public GmailPage close() {
		browser.click(CLOSE_BUTTON_LOCATOR);
		return new GmailPage();
	}

	public GmailPage send() {
		browser.click(SEND_BUTTON_LOCATOR);
		return new GmailPage();
	}
}
