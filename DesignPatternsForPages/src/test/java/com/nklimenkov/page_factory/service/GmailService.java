package test.java.com.nklimenkov.page_factory.service;

import test.java.com.nklimenkov.bo.Account;
import test.java.com.nklimenkov.bo.Message;
import test.java.com.nklimenkov.page_factory.pages.GmailDraftsPage;
import test.java.com.nklimenkov.page_factory.pages.GmailMessagePage;
import test.java.com.nklimenkov.page_factory.pages.GmailPage;
import test.java.com.nklimenkov.page_factory.pages.GmailWindowMessagePage;
import test.java.com.nklimenkov.page_factory.pages.SignInPage;

public class GmailService {

	private SignInPage signInPage;
	private GmailPage gmailPage;

	public GmailService() {
		signInPage = new SignInPage();
		gmailPage = new GmailPage();
	}

	public void signIn(Account account) {
		signInPage.open()
				.typeIdentifier(account.getIdentifier())
				.clickIdentifierNextButton()
				.typePassword(account.getPassword())
				.clickPasswordNextButton();
	}

	public void signOut() {
		gmailPage.clickAccountLink().clickAccountSignOutButton();
	}

	public boolean signInPageIsDisplayed() {
		return signInPage.isDisplayed();
	}

	public boolean gmailPageIsDisplayed() {
		return gmailPage.isDisplayed();
	}

	public void sendNewMessage(Message message) {
		GmailWindowMessagePage gmailWindowMessagePage = gmailPage.clickComposeButton();
		gmailWindowMessagePage.typeAddressee(message.getAddressee())
				.typeSubject(message.getSubject())
				.typeContent(message.getContent())
				.send();
	}

	public void createNewDraftMessage(Message message) {
		GmailWindowMessagePage gmailWindowMessagePage = gmailPage.clickComposeButton();
		gmailWindowMessagePage.typeAddressee(message.getAddressee())
				.typeSubject(message.getSubject())
				.typeContent(message.getContent()).close();
	}

	public void sendLatestDraftMessage() {
		GmailDraftsPage gmailDraftsPage = gmailPage.openDrafts();
		if (gmailDraftsPage.latestDraftMessageWillBeOpenedInWindow()) {
			gmailDraftsPage.openLatestDraftMessageWindow().send();
		} else {
			gmailDraftsPage.openLatestDraftMessage().send();
		}
	}

	public Message getLatestDraftMessage() {
		String addressee, subject, content;
		GmailDraftsPage gmailDraftsPage = gmailPage.openDrafts();
		if (gmailDraftsPage.latestDraftMessageWillBeOpenedInWindow()) {
			GmailWindowMessagePage gmailWindowMessagePage = gmailDraftsPage.openLatestDraftMessageWindow();
			addressee = gmailWindowMessagePage.getAddressee();
			subject = gmailWindowMessagePage.getSubject();
			content = gmailWindowMessagePage.getContent();
		} else {
			GmailMessagePage gmailMessagePage = gmailDraftsPage.openLatestDraftMessage();
			addressee = gmailMessagePage.getAddressee();
			subject = gmailMessagePage.getSubject();
			content = gmailMessagePage.getContent();
		}
		return new Message(addressee, subject, content);
	}

	public Message getLatestSentMessage() {
		GmailMessagePage gmailMessagePage = gmailPage.openSentMail().openLatestSentMessage();
		String addressee = gmailMessagePage.getAddressee();
		String subject = gmailMessagePage.getSubject();
		String content = gmailMessagePage.getContent();
		return new Message(addressee, subject, content);
	}
}
