package test.java.com.nklimenkov.pages;

import org.openqa.selenium.By;

public class DroppablePage extends MainPage {

	private static final By DRAGGABLE_ELEMENT_LOCATOR = By.id("draggable");
	private static final By DROPPABLE_ELEMENT_LOCATOR = By.id("droppable");
	private static final By DROPPABLE_ELEMENT_PARAGRAPH_LOCATOR = By.xpath("//div[@id='droppable']/p");

	public DroppablePage dragAndDropElement() {
		browser.dragAndDrop(DRAGGABLE_ELEMENT_LOCATOR, DROPPABLE_ELEMENT_LOCATOR);
		return this;
	}

	public String getTargetElementText() {
		return browser.getText(DROPPABLE_ELEMENT_PARAGRAPH_LOCATOR);
	}
}
