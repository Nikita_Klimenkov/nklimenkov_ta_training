package test.java.com.nklimenkov.stepdefs;

import org.apache.log4j.Logger;
import org.testng.Assert;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import test.java.com.nklimenkov.browser.Browser;
import test.java.com.nklimenkov.service.EbayService;

public class StepDefinitions {

	private static Logger logger = Logger.getLogger(StepDefinitions.class.getName());
	private EbayService ebayService = new EbayService();
	private Browser browser = new Browser();

	@Given("^I am on the www.ebay.com page$")
	public void i_am_on_the_ebay_dot_com_page() {
		try {
			logger.info("Opening www.ebay.com page");
			ebayService.openEbayMainPage();
			browser.takeScreenshot("Opened_ebay_dot_com_page");
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	@Given("^I switch to English language$")
	public void i_switch_to_English_language() {
		try {
			logger.info("Switching to English language");
			ebayService.switchLanguageToEnglish();
			ebayService.highlightCurrentLanguage();
			browser.takeScreenshot("Switched_to_English_language");
			ebayService.unHighlightCurrentLanguage();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	@When("^I search for \"([^\"]*)\"$")
	public void i_search_for(String searchQuery) {
		try {
			logger.info("Searching for \"" + searchQuery + "\"");
			ebayService.searchFor(searchQuery);
			browser.takeScreenshot("Performed_search");
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	@When("^I open page of search result product number \"(\\d)\"$")
	public void i_open_page_of_search_result_product_number(int num) {
		try {
			logger.info("Opening page of search result product number " + num);
			ebayService.openPageOfSearchResultProductNumber(num);
			browser.takeScreenshot("Opened_page_of_search_result_product_number_" + num);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	@When("^I add opened product to shopping cart$")
	public void i_add_opened_product_to_shopping_cart() {
		try {
			logger.info("Adding opened product to shopping cart");
			ebayService.addOpenedProductToShoppingCart();
			browser.takeScreenshot("Added_opened_product_to_shopping_cart");
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	@Then("^Search result number \"(\\d+)\" should case insensitive contain \"([^\"]*)\"$")
	public void search_result_number_should_case_insensitive_contain(int num, String query) {
		try {
			logger.info("Verifying search result number " + num + " to case insensitive contain \"" + query + "\"");
			String productName = ebayService.getNameOfSearchResultProductNumber(num);
			if (productName.equals("null")) {
				logger.warn("Product number " + num + " was not found on the search results page");
			}
			ebayService.highlightNameOfSearchResultProductNumber(num);
			browser.takeScreenshot(
					"Verifying_search_result_number_" + num + "_to_case_insensitive_contain_given_query");
			ebayService.unHighlightNameOfSearchResultProductNumber(num);
			Assert.assertTrue(productName.toLowerCase().contains(query.toLowerCase()));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	@Then("^Latest product in shopping cart should case insensitive contain \"([^\"]*)\"$")
	public void latest_product_in_shopping_cart_should_case_insensitive_contain(String query) {
		try {
			logger.info("Verifying latest product in shopping cart to case insensitive contain \"" + query + "\"");
			String shoppingCartLatestProductName = ebayService.getNameOfShoppingCartProductNumber(1);
			if (shoppingCartLatestProductName.equals("null")) {
				logger.warn("Latest product was not found in the shopping cart");
			}
			ebayService.highlightNameOfShoppingCartProductNumber(1);
			browser.takeScreenshot("Verifying_latest_product_in_shopping_cart_to_case_insensitive_contain_given_query");
			ebayService.unHighlightNameOfShoppingCartProductNumber(1);
			Assert.assertTrue(shoppingCartLatestProductName.toLowerCase().contains(query.toLowerCase()));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
}
