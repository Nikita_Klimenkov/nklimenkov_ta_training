package test.java.com.nklimenkov.page_factory.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import test.java.com.nklimenkov.bo.Account;

public class SignOutTest extends BaseTest {
	@Test
	public void testSignOut() {
		gmailService.signIn(Account.getDefaultAccount());
		gmailService.signOut();
		Assert.assertTrue(gmailService.signInPageIsDisplayed());
	}
}
