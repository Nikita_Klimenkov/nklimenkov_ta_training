package test.java.com.nklimenkov.page_factory.browser;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.concurrent.TimeUnit;

public class Browser {

	private static WebDriver driver;
	private static final int DEFAULT_TIMEOUT = 10;

	public Browser() {
		driver = getWebDriverInstance();
	}

	public static WebDriver getWebDriverInstance() {
		if (driver == null) {
			initializeWebDriver();
		}
		return driver;
	}

	private static void initializeWebDriver() {
		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(DEFAULT_TIMEOUT, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	public static void teardown() {
		if (driver != null) {
			driver.quit();
			driver = null;
		}
	}

	public void open(String URL) {
		driver.get(URL);
	}

	public String getTitle() {
		return driver.getTitle();
	}

	public String getCurrentUrl() {
		return driver.getCurrentUrl();
	}

	public WebElement findElementByXpath(String xpath) {
		return driver.findElement(By.xpath(xpath));
	}

	public boolean isPresentAndDisplayed(WebElement element) {
		try {
			return element.isDisplayed();
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public void wait(WebElement element) {
		new WebDriverWait(driver, DEFAULT_TIMEOUT).until(ExpectedConditions.elementToBeClickable(element));
	}

	public void click(WebElement element) {
		wait(element);
		element.click();
	}

	public void sendKeys(WebElement element, CharSequence keysToSend) {
		wait(element);
		element.sendKeys(keysToSend);
	}

	public String getText(WebElement element) {
		wait(element);
		return element.getText();
	}

	public String getAttribute(WebElement element, String attribute) {
		return element.getAttribute(attribute);
	}
}
