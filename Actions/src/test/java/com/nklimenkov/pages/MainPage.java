package test.java.com.nklimenkov.pages;

import org.openqa.selenium.By;
import test.java.com.nklimenkov.browser.Browser;

public class MainPage {

	protected Browser browser;

	private static final String URL = "http://jqueryui.com/";

	private static final By DEMO_FRAME_LOCATOR = By.className("demo-frame");
	private static final By DROPPABLE_PAGE_LINK_LOCATOR = By.linkText("Droppable");
	private static final By RESIZABLE_PAGE_LINK_LOCATOR = By.linkText("Resizable");
	private static final By SELECTABLE_PAGE_LINK_LOCATOR = By.linkText("Selectable");
	private static final By SORTABLE_PAGE_LINK_LOCATOR = By.linkText("Sortable");
	private static final By CHECKBOXRADIO_PAGE_LINK_LOCATOR = By.linkText("Checkboxradio");
	private static final By SELECTMENU_PAGE_LINK_LOCATOR = By.linkText("Selectmenu");
	private static final By TOOLTIP_PAGE_LINK_LOCATOR = By.linkText("Tooltip");

	public MainPage() {
		browser = new Browser();
	}

	public MainPage open() {
		browser.open(URL);
		return this;
	}

	public void switchToDemoFrame() {
		browser.switchToFrame(DEMO_FRAME_LOCATOR);
	}

	public DroppablePage openDroppablePage() {
		browser.click(DROPPABLE_PAGE_LINK_LOCATOR);
		return new DroppablePage();
	}

	public ResizablePage openResizablePage() {
		browser.click(RESIZABLE_PAGE_LINK_LOCATOR);
		return new ResizablePage();
	}

	public SelectablePage openSelectablePage() {
		browser.click(SELECTABLE_PAGE_LINK_LOCATOR);
		return new SelectablePage();
	}

	public SortablePage openSortablePage() {
		browser.click(SORTABLE_PAGE_LINK_LOCATOR);
		return new SortablePage();
	}

	public CheckboxRadioPage openCheckboxRadioPage() {
		browser.click(CHECKBOXRADIO_PAGE_LINK_LOCATOR);
		return new CheckboxRadioPage();
	}

	public SelectMenuPage openSelectMenuPage() {
		browser.click(SELECTMENU_PAGE_LINK_LOCATOR);
		return new SelectMenuPage();
	}

	public TooltipPage openTooltipPage() {
		browser.click(TOOLTIP_PAGE_LINK_LOCATOR);
		return new TooltipPage();
	}
}
