package test.java.com.nklimenkov.pages;

import org.openqa.selenium.By;

public class GmailDraftsPage extends GmailPage {

	private static final By TYPE_OF_LATEST_MESSAGE_LOCATOR = By
			.xpath("//div[@role='main']//tr[1]//div[@class='yW']/font");

	public boolean isLatestDraftMessageWillBeOpenedInWindow() {
		return browser.getText(TYPE_OF_LATEST_MESSAGE_LOCATOR).equals("Draft");
	}

	public GmailMessagePage openLatestDraftMessage() {
		openLatestMessage();
		return new GmailMessagePage();
	}

	public GmailWindowMessagePage openLatestDraftMessageWindow() {
		openLatestMessage();
		return new GmailWindowMessagePage();
	}
}
