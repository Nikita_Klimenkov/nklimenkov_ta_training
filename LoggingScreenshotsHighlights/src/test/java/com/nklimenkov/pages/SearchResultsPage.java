package test.java.com.nklimenkov.pages;

import org.openqa.selenium.By;

public class SearchResultsPage extends MainPage {

	private static final String SEARCH_RESULT_ITEM = "//div[@id='ResultSetItems']/ul/li[%s]/h3/a";

	public ProductPage openProductPageNumber(int num) {
		if (browser.isPresent(By.xpath(String.format(SEARCH_RESULT_ITEM, num)))) {
			browser.click(By.xpath(String.format(SEARCH_RESULT_ITEM, num)));
			return new ProductPage();
		} else {
			return null;
		}
	}

	public String getNameOfProductNumber(int num) {
		if (browser.isPresent(By.xpath(String.format(SEARCH_RESULT_ITEM, num)))) {
			return browser.getText(By.xpath(String.format(SEARCH_RESULT_ITEM, num)));
		} else {
			return "null";
		}
	}

	public SearchResultsPage highlightNameOfProductNumber(int num) {
		browser.highlightElement(By.xpath(String.format(SEARCH_RESULT_ITEM, num)), 5, "red");
		return this;
	}

	public SearchResultsPage unHighlightNameOfProductNumber(int num) {
		browser.unHighlightElement(By.xpath(String.format(SEARCH_RESULT_ITEM, num)));
		return this;
	}
}
