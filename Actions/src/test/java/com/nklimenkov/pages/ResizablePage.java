package test.java.com.nklimenkov.pages;

import org.openqa.selenium.By;

public class ResizablePage extends MainPage {

	private static final By RIGHT_HANDLE_LOCATOR = By.xpath("//div[@class='ui-resizable-handle ui-resizable-e']");
	private static final By BOTTOM_HANDLE_LOCATOR = By.xpath("//div[@class='ui-resizable-handle ui-resizable-s']");
	private static final By RIGHT_BOTTOM_HANDLE_LOCATOR = By
			.xpath("//div[@class='ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se']");
	private static final String scriptForGettingValueOfProperty = "var resizableElement = document.querySelector('#resizable'); "
			+ "return window.getComputedStyle(resizableElement, null).getPropertyValue('%s');";

	public ResizablePage resizeElementByRightHandle(int xOffset) {
		browser.dragAndDropBy(RIGHT_HANDLE_LOCATOR, xOffset, 0);
		return this;
	}

	public ResizablePage resizeElementByBottomHandle(int yOffset) {
		browser.dragAndDropBy(BOTTOM_HANDLE_LOCATOR, 0, yOffset);
		return this;
	}

	public ResizablePage resizeElementByRightBottomHandle(int xOffset, int yOffset) {
		browser.dragAndDropBy(RIGHT_BOTTOM_HANDLE_LOCATOR, xOffset, yOffset);
		return this;
	}

	public int getResizableElementWidth() {
		Object widthObject = browser.executeJS(String.format(scriptForGettingValueOfProperty, "width"));
		String widthValue = String.valueOf(widthObject);
		return Integer.parseInt(widthValue.substring(0, widthValue.length() - 2));
	}

	public int getResizableElementHeight() {
		Object heightObject = browser.executeJS(String.format(scriptForGettingValueOfProperty, "height"));
		String heightValue = String.valueOf(heightObject);
		return Integer.parseInt(heightValue.substring(0, heightValue.length() - 2));
	}
}
