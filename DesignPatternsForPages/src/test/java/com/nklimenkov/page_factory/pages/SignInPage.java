package test.java.com.nklimenkov.page_factory.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignInPage extends BasePage {

	private static final String URL = "https://mail.google.com";

	@FindBy(id = "logo")
	private WebElement logoDiv;

	@FindBy(id = "identifierId")
	private WebElement identifierTextbox;

	@FindBy(id = "identifierNext")
	private WebElement identifierNextButton;

	@FindBy(name = "password")
	private WebElement passwordTextbox;

	@FindBy(id = "passwordNext")
	private WebElement passwordNextButton;

	public SignInPage open() {
		browser.open(URL);
		return this;
	}

	public boolean isDisplayed() {
		browser.wait(logoDiv);
		return browser.getTitle().equals("Gmail") && browser.getCurrentUrl().contains("https://accounts.google.com")
				&& browser.isPresentAndDisplayed(logoDiv);
	}

	public SignInPage typeIdentifier(String identifier) {
		browser.sendKeys(identifierTextbox, identifier);
		return this;
	}

	public SignInPage clickIdentifierNextButton() {
		browser.click(identifierNextButton);
		return this;
	}

	public SignInPage typePassword(String password) {
		browser.sendKeys(passwordTextbox, password);
		return this;
	}

	public GmailPage clickPasswordNextButton() {
		browser.click(passwordNextButton);
		return new GmailPage();
	}
}
