package test.java.com.nklimenkov;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CalculatorSqrtTest extends BaseTest {

	@Test(dataProvider = "Data for sqrt test")
	public void testSqrt(double a, double expectedResult) {
		Assert.assertEquals(calculator.sqrt(a), expectedResult);
	}

	@DataProvider(name = "Data for sqrt test")
	public static Object[][] getDataForSqrtTest() {
		return new Object[][] { 
			{ 0, 0 }, { 1, 1 }, { -1, 1 }, 
			
			{ 0.25, 0.5 }, { -0.25, 0.5 },
			{ 9, 3 }, { -9, 3 }, 
			{ 4294967296d, 65536 }, { -4294967296d, 65536 }, 
			
			{ Long.MIN_VALUE, 3.03700049997605E9 }, { Long.MAX_VALUE, 3.03700049997605E9 }, 
			{ Double.MIN_VALUE, 2.2227587494850775E-162 }, { Double.MAX_VALUE, 1.3407807929942596E154 }, 
			{ Double.MIN_NORMAL, 1.4916681462400413E-154 } 
		};
	}
}
