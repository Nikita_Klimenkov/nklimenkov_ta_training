package test.java.com.nklimenkov.pages;

import org.openqa.selenium.By;

public class TooltipPage extends MainPage {

	private static final By TOOLTIPS_LINK_LOCATOR = By.linkText("Tooltips");
	private static final By THEMEROLLER_LINK_LOCATOR = By.linkText("ThemeRoller");
	private static final By INPUT_TEXTBOX_LOCATOR = By.id("age");
	private static final By LINKS_TOOLTIP_LOCATOR = By.xpath("//div[@role='tooltip']/div");

	public TooltipPage hoverMouseToTooltipsLink() {
		browser.moveToElement(TOOLTIPS_LINK_LOCATOR);
		return this;
	}

	public TooltipPage hoverMouseToThemeRollerLink() {
		browser.moveToElement(THEMEROLLER_LINK_LOCATOR);
		return this;
	}

	public TooltipPage hoverMouseToInputTextbox() {
		browser.moveToElement(INPUT_TEXTBOX_LOCATOR);
		return this;
	}

	public String getDisplayedTooltipText() {
		if (browser.isPresent(LINKS_TOOLTIP_LOCATOR)) {
			return browser.getText(LINKS_TOOLTIP_LOCATOR);
		} else {
			return null;
		}
	}
}
