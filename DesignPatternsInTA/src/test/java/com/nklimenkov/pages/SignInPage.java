package test.java.com.nklimenkov.pages;

import org.openqa.selenium.By;

public class SignInPage extends BasePage {

	private static final String URL = "https://mail.google.com";

	private static final By LOGO_LOCATOR = By.id("logo");
	private static final By IDENTIFIER_TEXTBOX_LOCATOR = By.id("identifierId");
	private static final By IDENTIFIER_NEXT_BUTTON_LOCATOR = By.id("identifierNext");
	private static final By PASSWORD_TEXTBOX_LOCATOR = By.name("password");
	private static final By PASSWORD_NEXT_BUTTON_LOCATOR = By.id("passwordNext");

	public SignInPage open() {
		browser.open(URL);
		return this;
	}

	public boolean isDisplayed() {
		browser.waitForElementToBeClickable(LOGO_LOCATOR);
		return browser.getTitle().equals("Gmail") && browser.getCurrentUrl().contains("https://accounts.google.com")
				&& browser.isDisplayed(LOGO_LOCATOR);
	}

	public SignInPage typeIdentifier(String identifier) {
		browser.sendKeys(IDENTIFIER_TEXTBOX_LOCATOR, identifier);
		return this;
	}

	public SignInPage clickIdentifierNextButton() {
		browser.click(IDENTIFIER_NEXT_BUTTON_LOCATOR);
		return this;
	}

	public SignInPage typePassword(String password) {
		browser.sendKeys(PASSWORD_TEXTBOX_LOCATOR, password);
		return this;
	}

	public GmailPage clickPasswordNextButton() {
		browser.click(PASSWORD_NEXT_BUTTON_LOCATOR);
		return new GmailPage();
	}
}
