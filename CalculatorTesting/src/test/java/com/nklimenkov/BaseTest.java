package test.java.com.nklimenkov;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import com.epam.tat.module4.Calculator;

public class BaseTest {

	protected Calculator calculator;

	@BeforeMethod
	public void setUp() {
		calculator = new Calculator();
	}

	@AfterMethod
	public void cleanUp() {
		calculator = null;
	}
}
