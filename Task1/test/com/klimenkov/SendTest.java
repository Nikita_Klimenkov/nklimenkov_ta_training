package com.klimenkov;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SendTest extends BaseConfiguration {
	
  @Test
  public void testSend() {
	  
		MailService.login();
		MailService.createDraft();
		MailService.openCreatedDraft();
		MailService.sendOpenedDraft();
		MailService.openLastSentLetter();
		
		//Assert.assertTrue(MailService.letterVerificationWithConversationViewOn());
		Assert.assertTrue(MailService.sentLetterVerificationWithConversationViewOff());
		
		MailService.logout();
  }
}
