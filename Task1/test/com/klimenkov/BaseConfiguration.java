package com.klimenkov;

import java.util.concurrent.TimeUnit;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BaseConfiguration {

	public static WebDriver driver;
	public static WebDriverWait wait;
	private static final String URL = "https://mail.google.com/mail/#inbox";

	@BeforeMethod
	public void setDriver() {
		if (driver == null) {
			System.setProperty("webdriver.chrome.driver", "C:\\MyData\\chromedriver_win32\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.get(URL);
			driver.manage().window().maximize();
			wait = new WebDriverWait(driver, 10);
		}
	}

	@AfterMethod
	public void quitDriver() {
		driver.quit();
		driver = null;
	}
}
