package test.java.com.nklimenkov.tests;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import test.java.com.nklimenkov.service.GmailService;

public class SignInTest extends BaseTest {

	private GmailService gmailService = new GmailService();

	@Test
	public void testSignIn() {
		gmailService.signIn(account);
		Assert.assertTrue(gmailService.gmailPageIsDisplayed());
	}

	@AfterMethod
	public void signOut() {
		gmailService.signOut();
	}
}
