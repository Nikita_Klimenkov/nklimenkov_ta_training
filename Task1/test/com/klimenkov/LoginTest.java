package com.klimenkov;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class LoginTest extends BaseConfiguration {

	@Test
	public void testLogin() {

		MailService.login();

		wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.ACCOUNT_ICON_LOCATOR));
		Assert.assertTrue(driver.findElement(Locators.ACCOUNT_ICON_LOCATOR).isDisplayed());
		
		MailService.logout();
	}
}
