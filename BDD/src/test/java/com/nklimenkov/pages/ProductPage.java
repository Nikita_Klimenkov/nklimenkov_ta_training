package test.java.com.nklimenkov.pages;

import org.openqa.selenium.By;

public class ProductPage extends MainPage {

	private static final By PRODUCT_NAME_LOCATOR = By.id("itemTitle");
	private static final By ADD_TO_CART_BUTTON_LOCATOR = By.id("isCartBtn_btn");

	public String getProductName() {
		return browser.getText(PRODUCT_NAME_LOCATOR);
	}

	public ShoppingCartPage addProductToShoppingCart() {
		browser.click(ADD_TO_CART_BUTTON_LOCATOR);
		return new ShoppingCartPage();
	}
}
