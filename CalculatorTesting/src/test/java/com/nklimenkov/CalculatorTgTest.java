package test.java.com.nklimenkov;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CalculatorTgTest extends BaseTest {

	// these test method is commented to get a successful build, uncomment it to see errors
//	@Test(dataProvider = "Data for tg test")
//	public void testTg(double a, double expectedResult) {
//		Assert.assertEquals(calculator.tg(a), expectedResult, 0.00001);
//	}

	@DataProvider(name = "Data for tg test")
	public static Object[][] getDataForTgTest() {
		return new Object[][] { 
			{ 0, 0 }, { 0.1, 0.10033467208545055 }, { -0.1, -0.10033467208545055 }, 
			{ 1, 1.5574077246549023 }, { -1, -1.5574077246549023 }, 
			
			{ Math.toRadians(30), 0.5773502691896257 }, { Math.toRadians(45), 1 }, 
			{ Math.toRadians(60), 1.7320508075688767 }, { Math.toRadians(90), 1.633123935319537E16 }, 
			{ Math.toRadians(120), -1.7320508075688783 }, { Math.toRadians(180), 0 }, 
			{ Math.toRadians(270), 5.443746451065123E15 }, { Math.toRadians(360), 0 }, 
			
			{ Long.MIN_VALUE, -84.73931296875567 }, { Long.MAX_VALUE, 84.73931296875567 }, 
			{ Double.MIN_VALUE, 4.9E-324 }, { Double.MAX_VALUE, -0.004962015874444895 }, 
			{ Double.MIN_NORMAL, 2.2250738585072014E-308 } 
		};
	}
}
