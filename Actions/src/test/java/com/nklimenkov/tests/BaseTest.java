package test.java.com.nklimenkov.tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import test.java.com.nklimenkov.browser.Browser;
import test.java.com.nklimenkov.pages.MainPage;

public class BaseTest {

	protected MainPage mainPage;

	@BeforeMethod
	public void setUp() {
		mainPage = new MainPage();
		mainPage.open();
	}

	@AfterMethod
	public void cleanUp() {
		Browser.teardown();
	}
}
