package com.klimenkov;

import org.testng.Assert;
import org.testng.annotations.Test;

public class DraftTest extends BaseConfiguration {
	
	@Test
	public void testDraft() {

		MailService.login();
		MailService.createDraft();
		MailService.openCreatedDraft();
		
		//Assert.assertTrue(MailService.letterVerificationWithConversationViewOn());
		Assert.assertTrue(MailService.draftLetterVerificationWithConversationViewOff());
		
		MailService.logout();
	}
}
