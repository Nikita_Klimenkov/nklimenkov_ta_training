package test.java.com.nklimenkov.pages;

import test.java.com.nklimenkov.browser.Browser;

public class BasePage {

	protected Browser browser;

	public BasePage() {
		browser = new Browser();
	}
}
