package test.java.com.nklimenkov.pages;

import java.util.ArrayList;
import org.openqa.selenium.By;

public class CheckboxRadioPage extends MainPage {

	private static final String RADIO_BUTTON_XPATH = "//label[@for='radio-%s']";
	private static final String CHECKBOX_XPATH = "//label[@for='checkbox-%s']";
	private static final String CHECKBOX_IN_LABEL_XPATH = "//label[@for='checkbox-nested-%s']";

	public CheckboxRadioPage selectRadioButton(int button) {
		browser.click(By.xpath(String.format(RADIO_BUTTON_XPATH, button)));
		return this;
	}

	public CheckboxRadioPage selectCheckboxes(int[] checkboxes) {
		for (int i = 0; i < checkboxes.length; i++) {
			browser.click(By.xpath(String.format(CHECKBOX_XPATH, checkboxes[i])));
		}
		return this;
	}

	public CheckboxRadioPage deselectAllCheckboxes() {
		for (int i = 1; i <= 4; i++) {
			By checkboxLocator = By.xpath(String.format(CHECKBOX_XPATH, i));
			String classValue = browser.getAttribute(checkboxLocator, "class");
			if (classValue.contains("checked")) {
				browser.click(checkboxLocator);
			}
		}
		return this;
	}

	public CheckboxRadioPage selectCheckboxesNestedInLabels(int[] checkboxes) {
		for (int i = 0; i < checkboxes.length; i++) {
			browser.click(By.xpath(String.format(CHECKBOX_IN_LABEL_XPATH, checkboxes[i])));
		}
		return this;
	}

	public CheckboxRadioPage deselectAllCheckboxesNestedInLabels() {
		for (int i = 1; i <= 4; i++) {
			By checkboxLocator = By.xpath(String.format(CHECKBOX_IN_LABEL_XPATH, i));
			String classValue = browser.getAttribute(checkboxLocator, "class");
			if (classValue.contains("checked")) {
				browser.click(checkboxLocator);
			}
		}
		return this;
	}

	public int getSelectedRadioButton() {
		for (int i = 1; i <= 3; i++) {
			By buttonLocator = By.xpath(String.format(RADIO_BUTTON_XPATH, i));
			String classValue = browser.getAttribute(buttonLocator, "class");
			if (classValue.contains("active")) {
				return i;
			}
		}
		return -1;
	}

	public int[] getSelectedCheckboxes() {
		ArrayList<Integer> selectedCheckboxesList = new ArrayList<Integer>();
		for (int i = 1; i <= 4; i++) {
			By checkboxLocator = By.xpath(String.format(CHECKBOX_XPATH, i));
			String classValue = browser.getAttribute(checkboxLocator, "class");
			if (classValue.contains("checked")) {
				selectedCheckboxesList.add(i);
			}
		}
		int[] selectedCheckboxes = new int[selectedCheckboxesList.size()];
		for (int i = 0; i < selectedCheckboxes.length; i++) {
			selectedCheckboxes[i] = selectedCheckboxesList.get(i).intValue();
		}
		return selectedCheckboxes;
	}

	public int[] getSelectedCheckboxesNestedInLabels() {
		ArrayList<Integer> selectedCheckboxesList = new ArrayList<Integer>();
		for (int i = 1; i <= 4; i++) {
			By checkboxLocator = By.xpath(String.format(CHECKBOX_IN_LABEL_XPATH, i));
			String classValue = browser.getAttribute(checkboxLocator, "class");
			if (classValue.contains("active")) {
				selectedCheckboxesList.add(i);
			}
		}
		int[] selectedCheckboxes = new int[selectedCheckboxesList.size()];
		for (int i = 0; i < selectedCheckboxes.length; i++) {
			selectedCheckboxes[i] = selectedCheckboxesList.get(i).intValue();
		}
		return selectedCheckboxes;
	}
}
