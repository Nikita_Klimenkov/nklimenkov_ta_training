package test.java.com.nklimenkov.page_object.pages;

import org.openqa.selenium.By;

public class GmailMessagePage extends GmailPage {

	private static final By ADDRESSEE_SPAN_LOCATOR = By.xpath("//span[@class='g2']");
	private static final By SUBJECT_HEADER_LOCATOR = By.xpath("//h2[@class='hP']");
	private static final By CONTENT_DIV_WITH_CONVERSATION_VIEW_ON_LOCATOR = By
			.xpath("//div[@class='Am aO9 Al editable LW-avf']");
	private static final By CONTENT_DIV_WITH_CONVERSATION_VIEW_OFF_LOCATOR = By
			.xpath("//div[@class='ii gt ']/div/div[@dir='ltr']");
	private static final By SEND_BUTTON_LOCATOR = By.xpath("//div[@class='T-I J-J5-Ji aoO T-I-atl L3']");

	public String getAddressee() {
		return browser.getAttribute(ADDRESSEE_SPAN_LOCATOR, "email");
	}

	public String getSubject() {
		return browser.getText(SUBJECT_HEADER_LOCATOR);
	}

	public String getContent() {
		if (browser.isPresent(CONTENT_DIV_WITH_CONVERSATION_VIEW_ON_LOCATOR)) {
			return browser.getText(CONTENT_DIV_WITH_CONVERSATION_VIEW_ON_LOCATOR);
		} else {
			return browser.getText(CONTENT_DIV_WITH_CONVERSATION_VIEW_OFF_LOCATOR);
		}
	}

	public GmailMessagePage send() {
		browser.click(SEND_BUTTON_LOCATOR);
		return this;
	}
}
