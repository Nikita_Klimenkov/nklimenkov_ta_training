package test.java.com.nklimenkov;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;

public class CalculatorSumTest extends BaseTest {

	@Test(dataProvider = "Data for sum test with Long")
	public void testSumWithLong(long a, long b, long expectedResult) {
		Assert.assertEquals(calculator.sum(a, b), expectedResult);
	}

	@Test(dataProvider = "Data for sum test with Double")
	public void testSumWithDouble(double a, double b, double expectedResult) {
		Assert.assertEquals(calculator.sum(a, b), expectedResult);
	}

	@DataProvider(name = "Data for sum test with Long")
	public static Object[][] getDataForSumTestWithLong() {
		return new Object[][] { 
			{ 0, 0, 0 }, { 0, 1, 1 }, { 0, -1, -1 }, 
			{ 1, 0, 1 }, { 1, 1, 2 }, { 1, -1, 0 }, 
			{ -1, 0, -1 }, { -1, 1, 0 }, { -1, -1, -2 },
			
			{ 5, 10, 15 }, { 5, -10, -5 }, { -5, 10, 5 }, { -5, -10, -15 }, 
			{ 1, 1000000, 1000001 }, { 1, -1000000, -999999 }, { -1, 1000000, 999999 }, { -1, -1000000, -1000001 }, 
			
			{ Long.MIN_VALUE, 0, Long.MIN_VALUE }, { Long.MAX_VALUE, 0, Long.MAX_VALUE } 
		};
	}

	@DataProvider(name = "Data for sum test with Double")
	public static Object[][] getDataForSumTestWithDouble() {
		return new Object[][] { 
			{ 0, 0, 0 }, { 0, 0.1, 0.1 }, { 0, -0.1, -0.1 }, 
			{ 0.1, 0, 0.1 }, { 0.1, 0.1, 0.2 }, { 0.1, -0.1, 0 }, 
			{ -0.1, 0, -0.1 }, { -0.1, 0.1, 0 }, { -0.1, -0.1, -0.2 }, 
			
			{ 5, 10, 15 }, { 5, -10, -5 }, { -5, 10, 5 }, { -5, -10, -15 }, 
			{ 1, 1000000, 1000001 }, { 1, -1000000, -999999 }, { -1, 1000000, 999999 }, { -1, -1000000, -1000001 }, 
			
			{ Long.MIN_VALUE, 0, Long.MIN_VALUE }, { Long.MAX_VALUE, 0, Long.MAX_VALUE }, 
			{ Double.MIN_VALUE, 0, Double.MIN_VALUE }, { Double.MAX_VALUE, 0, Double.MAX_VALUE }, 
			{ Double.MIN_NORMAL, 0, Double.MIN_NORMAL } 
		};
	}
}
