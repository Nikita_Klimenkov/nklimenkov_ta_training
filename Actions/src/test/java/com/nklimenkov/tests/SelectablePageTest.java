package test.java.com.nklimenkov.tests;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import test.java.com.nklimenkov.pages.SelectablePage;

public class SelectablePageTest extends BaseTest {

	private SelectablePage selectablePage;

	@BeforeMethod
	public void setUpSelectablePageTest() {
		selectablePage = mainPage.openSelectablePage();
		selectablePage.switchToDemoFrame();
	}

	@Test
	public void testSelectItem() {
		for (int i = 1; i <= 7; i++) {
			selectablePage.selectItem(i);
			Assert.assertEquals(selectablePage.getSelectedItems(), new int[] { i });
		}
	}

	@Test
	public void testSelectItems() {
		int[] itemsToSelect = { 1, 3, 5, 7 };
		selectablePage.selectItems(itemsToSelect);
		Assert.assertEquals(selectablePage.getSelectedItems(), itemsToSelect);
	}

	@Test
	public void testSelectItemsFromTo() {
		selectablePage.selectItemsFromTo(2, 6);
		int[] itemsExpectedToBeSelected = { 2, 3, 4, 5, 6 };
		Assert.assertEquals(selectablePage.getSelectedItems(), itemsExpectedToBeSelected);
	}
}
