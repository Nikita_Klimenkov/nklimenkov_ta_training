package test.java.com.nklimenkov.tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import test.java.com.nklimenkov.bo.Account;
import test.java.com.nklimenkov.bo.AccountFactory;
import test.java.com.nklimenkov.browser.Browser;

public class BaseTest {

	protected Account account;

	@BeforeMethod
	public void setUp() {
		account = AccountFactory.getDefaultAccount();
	}

	@AfterMethod
	public void cleanUp() {
		Browser.teardown();
	}
}
