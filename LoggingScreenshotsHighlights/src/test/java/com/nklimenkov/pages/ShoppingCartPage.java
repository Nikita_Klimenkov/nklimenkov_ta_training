package test.java.com.nklimenkov.pages;

import org.openqa.selenium.By;

public class ShoppingCartPage extends MainPage {

	private static final String SHOPPING_CART_ITEM = "//div[@id='ShopCart']/div/div[%s]/div/div[2]//span/a";

	public String getNameOfProductNumber(int num) {
		if (browser.isPresent(By.xpath(String.format(SHOPPING_CART_ITEM, num)))) {
			return browser.getText(By.xpath(String.format(SHOPPING_CART_ITEM, num)));
		} else {
			return "null";
		}
	}

	public ShoppingCartPage highlightNameOfProductNumber(int num) {
		browser.highlightElement(By.xpath(String.format(SHOPPING_CART_ITEM, num)), 5, "red");
		return this;
	}

	public ShoppingCartPage unHighlightNameOfProductNumber(int num) {
		browser.unHighlightElement(By.xpath(String.format(SHOPPING_CART_ITEM, num)));
		return this;
	}
}
