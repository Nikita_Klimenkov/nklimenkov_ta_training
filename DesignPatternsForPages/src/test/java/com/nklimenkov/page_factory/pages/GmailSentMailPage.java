package test.java.com.nklimenkov.page_factory.pages;

public class GmailSentMailPage extends GmailPage {

	public GmailMessagePage openLatestSentMessage() {
		openLatestMessage();
		return new GmailMessagePage();
	}
}
