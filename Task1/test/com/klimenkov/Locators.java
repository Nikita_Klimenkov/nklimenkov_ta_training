package com.klimenkov;

import org.openqa.selenium.By;

public class Locators {

	public static final By IDENTIFIER_TEXTBOX_LOCATOR = By.id("identifierId");
	public static final By PASSWORD_TEXTBOX_LOCATOR = By.xpath("//input[@type='password']");
	public static final By ACCOUNT_ICON_LOCATOR = By.cssSelector(".gb_b.gb_fb.gb_R");
	public static final By ACCOUNT_EXIT_BUTTON_LOCATOR = By.xpath("//a[@class='gb_Ea gb_Qf gb_Xf gb_Ee gb_Db']");
	public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//div[@class='T-I J-J5-Ji T-I-KE L3']");
	public static final By ADDRESSEE_TEXTBOX_LOCATOR = By.xpath("//textarea[@name='to']");
	public static final By ADDRESSEE_SPAN_LOCATOR = By.xpath("//span[@class='g2']");
	public static final By ADDRESSEE_DRAFT_TEXTBOX_LOCATOR = By.xpath("//div[@class='oL aDm az9']/span");
	public static final By SUBJECT_TEXTBOX_LOCATOR = By.xpath("//input[@name='subjectbox']");
	public static final By SUBJECT_HEADER_LOCATOR = By.xpath("//h2[@class='hP']");
	public static final By SUBJECT_DRAFT_TEXTBOX_LOCATOR = By.xpath("//table[@class='aoP aoC']//input[@name='subject']");
	public static final By CONTENT_TEXTBOX_LOCATOR = By.xpath("//div[@class='Am Al editable LW-avf']");
	public static final By CONTENT_DIV_CONVERSATION_VIEW_ON_LOCATOR = By.xpath("//div[@class='Am aO9 Al editable LW-avf']");
	public static final By CONTENT_DIV_CONVERSATION_VIEW_OFF_LOCATOR = By.xpath("//div[@class='ii gt ']/div/div[@dir='ltr']");
	public static final By SEND_BUTTON_LOCATOR = By.xpath("//div[@class='T-I J-J5-Ji aoO T-I-atl L3']");
	public static final By CLOSE_BUTTON_LOCATOR = By.xpath("//img[@class='Ha']");
	public static final By SENT_MAIL_BUTTON_LOCATOR = By.xpath("//div[@class='TN GLujEb aHS-bnu']//a");
	public static final By HIGHLITED_SENT_MAIL_BUTTON_LOCATOR = By.xpath("//div[@class='TN GLujEb aHS-bnu']//a[@tabindex='0']");
	public static final By DRAFTS_BUTTON_LOCATOR = By.xpath("//div[@class='TN GLujEb aHS-bnq']//a");
	public static final By HIGHLITED_DRAFTS_BUTTON_LOCATOR = By.xpath("//div[@class='TN GLujEb aHS-bnq']//a[@tabindex='0']");
	public static final By LATEST_LETTER_LINK_LOCATOR = By.xpath("//div[@role='main']//table[@class='F cf zt']/tbody/tr[1]/td[@class='xY a4W']//div[@class='y6']");
}
