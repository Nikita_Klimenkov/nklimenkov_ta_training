@searchProduct
Feature: Search product

  Background: 
    Given I am on the www.ebay.com page
    And I switch to English language

  Scenario: Search product in ebay
    When I search for "piano"
    Then Search result number "1" should case insensitive contain "piano"

  Scenario Outline: Search products in ebay
    When I search for "<search query>"
    Then Search result number "<search result number>" should case insensitive contain "<search query>"

    Examples: 
      | search query | search result number |
      | Harry Potter |                    2 |
      | AIRPODS      |                    3 |
