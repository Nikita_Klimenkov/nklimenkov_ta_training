package test.java.com.nklimenkov;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

public class CalculatorIsPositiveIsNegativeTest extends BaseTest {

	private long parameter;
	boolean isPositiveExpectedResult;
	boolean isNegativeExpectedResult;

	@Factory(dataProvider = "Data for isPositive isNegative tests")
	public CalculatorIsPositiveIsNegativeTest(long parameter, boolean isPositiveExpectedResult,
			boolean isNegativeExpectedResult) {
		this.parameter = parameter;
		this.isPositiveExpectedResult = isPositiveExpectedResult;
		this.isNegativeExpectedResult = isNegativeExpectedResult;
	}

	@Test
	public void testIsPositive() {
		Assert.assertEquals(calculator.isPositive(parameter), isPositiveExpectedResult);
	}

	@Test
	public void testIsNegative() {
		Assert.assertEquals(calculator.isNegative(parameter), isNegativeExpectedResult);
	}

	@DataProvider(name = "Data for isPositive isNegative tests")
	public static Object[][] getDataForSumTestWithLong() {
		return new Object[][] { 
			{ 0, false, false }, { 1, true, false }, { -1, false, true }, 
			{ 10, true, false }, { -10, false, true }, 
			{ 999999, true, false }, { -999999, false, true }, 
			{ Long.MIN_VALUE, false, true }, { Long.MAX_VALUE, true, false } 
		};
	}
}
