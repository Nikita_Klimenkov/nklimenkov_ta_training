package test.java.com.nklimenkov.bo;

public class Account {

	private String identifier;
	private String password;

	public Account(String identifier, String password) {
		this.identifier = identifier;
		this.password = password;
	}

	public static Account getDefaultAccount() {
		return new Account("NikitaKlimenkovHere@gmail.com", "supersecurepassword");
	}

	public String getIdentifier() {
		return identifier;
	}

	public String getPassword() {
		return password;
	}
}
