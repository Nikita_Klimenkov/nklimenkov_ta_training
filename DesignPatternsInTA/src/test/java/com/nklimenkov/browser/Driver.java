package test.java.com.nklimenkov.browser;

import org.openqa.selenium.WebDriver;

public interface Driver {

	public WebDriver getTargetDriver();
}
