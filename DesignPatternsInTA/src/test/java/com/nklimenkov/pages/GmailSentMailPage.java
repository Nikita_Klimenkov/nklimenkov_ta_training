package test.java.com.nklimenkov.pages;

public class GmailSentMailPage extends GmailPage {

	public GmailMessagePage openLatestSentMessage() {
		openLatestMessage();
		return new GmailMessagePage();
	}
}
