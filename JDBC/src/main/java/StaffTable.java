package main.java;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StaffTable extends DatabaseConfigurations {

    public static ResultSet getResultSet() {
        try {
            resultSet = statement.executeQuery("SELECT * FROM staff");
            return resultSet;
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
    }

    public static void addEmployee(int id, String name, String surname) {
        try {
            preparedStatement = connection.prepareStatement("INSERT INTO staff VALUES (?, ?, ?)");
            preparedStatement.setInt(1, id);
            preparedStatement.setString(2, name);
            preparedStatement.setString(3, surname);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public static void removeEmployeeWithId(int id) {
        try {
            preparedStatement = connection.prepareStatement("DELETE FROM staff WHERE id = ?");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public static boolean containsEmployeeWithId(int id) {
        try {
            preparedStatement = connection.prepareStatement("SELECT * FROM staff WHERE id = ?");
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
    }
}
