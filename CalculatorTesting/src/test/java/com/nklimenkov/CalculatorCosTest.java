package test.java.com.nklimenkov;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CalculatorCosTest extends BaseTest {

	// these test method is commented to get a successful build, uncomment it to see errors
//	@Test(dataProvider = "Data for cos test")
//	public void testCos(double a, double expectedResult) {
//		Assert.assertEquals(calculator.cos(a), expectedResult, 0.00001);
//	}

	@DataProvider(name = "Data for cos test")
	public static Object[][] getDataForCosTest() {
		return new Object[][] { 
			{ 0, 1 }, { 0.1, 0.9950041652780258 }, { -0.1, 0.9950041652780258 }, 
			{ 1, 0.5403023058681398 }, { -1, 0.5403023058681398 }, 
			
			{ Math.toRadians(30), 0.8660254037844386 }, { Math.toRadians(45), 0.7071067811865475 }, 
			{ Math.toRadians(60), 0.5 }, { Math.toRadians(90), 0 }, 
			{ Math.toRadians(120), -0.5 }, { Math.toRadians(180), -1 }, 
			{ Math.toRadians(270), 0 }, { Math.toRadians(360), 1 }, 
			
			{ Long.MIN_VALUE, 0.011800076512800236 }, { Long.MAX_VALUE, 0.011800076512800236 }, 
			{ Double.MIN_VALUE, 1 }, { Double.MAX_VALUE, -0.9999876894265599 }, 
			{ Double.MIN_NORMAL, 1 } 
		};
	}
}
