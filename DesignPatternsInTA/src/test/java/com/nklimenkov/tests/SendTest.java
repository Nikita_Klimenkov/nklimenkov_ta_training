package test.java.com.nklimenkov.tests;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import test.java.com.nklimenkov.bo.Message;
import test.java.com.nklimenkov.bo.MessageFactory;
import test.java.com.nklimenkov.service.GmailService;

public class SendTest extends BaseTest {

	private GmailService gmailService = new GmailService();

	@Test
	public void testSendNewMessage() {
		gmailService.signIn(account);
		gmailService.sendNewMessage(MessageFactory.getDefaultMessage());
		Message sentMessage = gmailService.getLatestSentMessage();
		Assert.assertTrue(sentMessage.equals(MessageFactory.getDefaultMessage()));
	}

	@Test
	public void testCreateAndSendNewDraftMessage() {
		gmailService.signIn(account);
		gmailService.createNewDraftMessage(MessageFactory.getDefaultMessage());
		gmailService.sendLatestDraftMessage();
		Message sentMessage = gmailService.getLatestSentMessage();
		Assert.assertTrue(sentMessage.equals(MessageFactory.getDefaultMessage()));
	}

	@AfterMethod
	public void signOut() {
		gmailService.signOut();
	}
}
