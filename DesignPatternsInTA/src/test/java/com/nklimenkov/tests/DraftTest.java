package test.java.com.nklimenkov.tests;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import test.java.com.nklimenkov.bo.Message;
import test.java.com.nklimenkov.bo.MessageFactory;
import test.java.com.nklimenkov.service.GmailService;

public class DraftTest extends BaseTest {

	private GmailService gmailService = new GmailService();

	@Test
	public void testCreateNewDraftMessage() {
		gmailService.signIn(account);
		gmailService.createNewDraftMessage(MessageFactory.getDefaultMessage());
		Message draftMessage = gmailService.getLatestDraftMessage();
		Assert.assertTrue(draftMessage.equals(MessageFactory.getDefaultMessage()));
	}

	@AfterMethod
	public void signOut() {
		gmailService.signOut();
	}
}
