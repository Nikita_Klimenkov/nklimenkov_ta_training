package test.java.com.nklimenkov.tests;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import test.java.com.nklimenkov.pages.TooltipPage;

public class TooltipPageTest extends BaseTest {

	private TooltipPage tooltipPage;

	@BeforeMethod
	public void setUpTooltipPageTest() {
		tooltipPage = mainPage.openTooltipPage();
		tooltipPage.switchToDemoFrame();
	}

	@Test
	public void testGetDisplayedTooltipTextWithTooltipsLink() {
		tooltipPage.hoverMouseToTooltipsLink();
		String expectedTooltip = "That's what this widget is";
		Assert.assertEquals(tooltipPage.getDisplayedTooltipText(), expectedTooltip);
	}

	@Test
	public void testGetDisplayedTooltipTextWithThemeRollerLink() {
		tooltipPage.hoverMouseToThemeRollerLink();
		String expectedTooltip = "ThemeRoller: jQuery UI's theme builder application";
		Assert.assertEquals(tooltipPage.getDisplayedTooltipText(), expectedTooltip);
	}

	@Test
	public void testGetDisplayedTooltipTextWithInputTextbox() {
		tooltipPage.hoverMouseToInputTextbox();
		String expectedTooltip = "We ask for your age only for statistical purposes.";
		Assert.assertEquals(tooltipPage.getDisplayedTooltipText(), expectedTooltip);
	}
}
