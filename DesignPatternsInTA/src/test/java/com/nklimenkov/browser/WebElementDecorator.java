package test.java.com.nklimenkov.browser;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebElementDecorator implements WebElement {

	private WebElement element;
	private WebDriver driver;
	private static final int DEFAULT_TIMEOUT = 10;

	public WebElementDecorator(WebElement element, WebDriver driver) {
		this.element = element;
		this.driver = driver;
	}

	private void waitForElementToBeClickable() {
		new WebDriverWait(driver, DEFAULT_TIMEOUT).until(ExpectedConditions.elementToBeClickable(element));
	}

	@Override
	public <X> X getScreenshotAs(OutputType<X> arg0) throws WebDriverException {
		return element.getScreenshotAs(arg0);
	}

	@Override
	public void clear() {
		waitForElementToBeClickable();
		element.clear();
	}

	@Override
	public void click() {
		waitForElementToBeClickable();
		element.click();
	}

	@Override
	public WebElement findElement(By arg0) {
		return element.findElement(arg0);
	}

	@Override
	public List<WebElement> findElements(By arg0) {
		return element.findElements(arg0);
	}

	@Override
	public String getAttribute(String arg0) {
		return element.getAttribute(arg0);
	}

	@Override
	public String getCssValue(String arg0) {
		return element.getCssValue(arg0);
	}

	@Override
	public Point getLocation() {
		return element.getLocation();
	}

	@Override
	public Rectangle getRect() {
		return element.getRect();
	}

	@Override
	public Dimension getSize() {
		return element.getSize();
	}

	@Override
	public String getTagName() {
		return element.getTagName();
	}

	@Override
	public String getText() {
		waitForElementToBeClickable();
		return element.getText();
	}

	@Override
	public boolean isDisplayed() {
		return element.isDisplayed();
	}

	@Override
	public boolean isEnabled() {
		return element.isEnabled();
	}

	@Override
	public boolean isSelected() {
		return element.isSelected();
	}

	@Override
	public void sendKeys(CharSequence... arg0) {
		waitForElementToBeClickable();
		element.sendKeys(arg0);
	}

	@Override
	public void submit() {
		element.submit();
	}
}
