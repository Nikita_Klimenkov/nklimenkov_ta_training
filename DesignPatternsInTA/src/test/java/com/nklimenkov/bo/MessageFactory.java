package test.java.com.nklimenkov.bo;

public class MessageFactory {

	private static final String ADDRESSEE = "Mikita_Klimiankou@epam.com";
	private static final String SUBJECT = "Test";
	private static final String CONTENT = "Selenium Driver Test";

	public static Message getDefaultMessage() {
		return new Message(ADDRESSEE, SUBJECT, CONTENT);
	}
}
