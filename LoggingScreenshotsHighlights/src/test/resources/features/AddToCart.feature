@addToCart
Feature: Add to cart

  Scenario Outline: Search and add to shopping cart products in ebay
    Given I am on the www.ebay.com page
    And I switch to English language
    When I search for "<search query>"
    And I open page of search result product number "<search result number>"
    And I add opened product to shopping cart
    Then Latest product in shopping cart should case insensitive contain "<search query>"

    Examples: 
      | search query     | search result number |
      | Thinking in Java |                    1 |
