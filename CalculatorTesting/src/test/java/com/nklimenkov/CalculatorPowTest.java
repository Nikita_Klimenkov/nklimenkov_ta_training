package test.java.com.nklimenkov;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CalculatorPowTest extends BaseTest {

	@Test(dataProvider = "Data for pow test")
	public void testPow(double a, double b, double expectedResult) {
		Assert.assertEquals(calculator.pow(a, b), expectedResult);
	}

	@DataProvider(name = "Data for pow test")
	public static Object[][] getDataForPowTest() {
		return new Object[][] {
			{ 0, 0, 1 }, { 0, 0.1, 1 }, { 0, -0.1, Double.POSITIVE_INFINITY }, 
			{ 0.1, 0, 1 }, { 0.1, 0.1, 1 }, { 0.1, -0.1, 10 }, 
			{ -0.1, 0, 1 }, { -0.1, 0.1, 1 }, { -0.1, -0.1, -10 }, 

			{ 0, 1, 0 }, { 0, -1, Double.POSITIVE_INFINITY }, 
			{ 1, 0, 1 }, { 1, 1, 1 }, { 1, -1, 1 }, 
			{ -1, 0, 1 }, { -1, 1, -1 }, { -1, -1, -1 }, 

			{ 4, 4, 256 }, { 4, -4, 0.00390625 }, { -4, 4, 256 }, { -4, -4, 0.00390625 }, 

			{ Long.MIN_VALUE, 1, Long.MIN_VALUE }, { Long.MAX_VALUE, 1, Long.MAX_VALUE }, 
			{ Double.MIN_VALUE, 1, Double.MIN_VALUE }, { Double.MAX_VALUE, 1, Double.MAX_VALUE }, 
			{ Double.MIN_NORMAL, 1, Double.MIN_NORMAL } 
		};
	}
}
