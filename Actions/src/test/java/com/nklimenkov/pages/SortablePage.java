package test.java.com.nklimenkov.pages;

import org.openqa.selenium.By;

public class SortablePage extends MainPage {

	private static final String ITEM_XPATH = "//ul[@id='sortable']/li[%s]";

	public int[] getOrderOfItems() {
		int[] orderOfItems = new int[7];
		for (int i = 1; i <= 7; i++) {
			By itemLocator = By.xpath(String.format(ITEM_XPATH, i));
			String itemText = browser.getText(itemLocator);
			orderOfItems[i - 1] = Integer.parseInt(itemText.substring(itemText.length() - 1));
		}
		return orderOfItems;
	}

	public SortablePage moveItemFromTo(int sourcePlace, int targetPlace) {
		By sourceItemLocator = By.xpath(String.format(ITEM_XPATH, sourcePlace));
		By targetItemLocator = By.xpath(String.format(ITEM_XPATH, targetPlace));
		if (sourcePlace < targetPlace) {
			browser.dragAndDropBy(sourceItemLocator, targetItemLocator, 200, 30);
		} else {
			browser.dragAndDropBy(sourceItemLocator, targetItemLocator, 200, 10);
		}
		return this;
	}
}
