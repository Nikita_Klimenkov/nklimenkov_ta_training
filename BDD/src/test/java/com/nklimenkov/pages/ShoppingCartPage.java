package test.java.com.nklimenkov.pages;

import org.openqa.selenium.By;

public class ShoppingCartPage extends MainPage {

	private static final String SHOPPING_CART_ITEM = "//div[@id='ShopCart']/div/div[%s]/div/div[2]//span/a";

	public String getNameOfProductNumber(int i) {
		if (browser.isPresent(By.xpath(String.format(SHOPPING_CART_ITEM, i)))) {
			return browser.getText(By.xpath(String.format(SHOPPING_CART_ITEM, i)));
		} else {
			return "null";
		}
	}
}
