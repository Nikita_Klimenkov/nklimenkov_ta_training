package test.java.com.nklimenkov.service;

import test.java.com.nklimenkov.bo.Account;
import test.java.com.nklimenkov.bo.Message;
import test.java.com.nklimenkov.pages.GmailDraftsPage;
import test.java.com.nklimenkov.pages.GmailMessagePage;
import test.java.com.nklimenkov.pages.GmailPage;
import test.java.com.nklimenkov.pages.GmailWindowMessagePage;
import test.java.com.nklimenkov.pages.SignInPage;

public class GmailService {

	private SignInPage signInPage = new SignInPage();
	private GmailPage gmailPage = new GmailPage();

	public void signIn(Account account) {
		signInPage.open()
				.typeIdentifier(account.getIdentifier())
				.clickIdentifierNextButton()
				.typePassword(account.getPassword())
				.clickPasswordNextButton();
	}

	public void signOut() {
		gmailPage.clickAccountLink().clickAccountSignOutButton();
	}

	public boolean signInPageIsDisplayed() {
		return signInPage.isDisplayed();
	}

	public boolean gmailPageIsDisplayed() {
		return gmailPage.isDisplayed();
	}

	public void sendNewMessage(Message message) {
		GmailWindowMessagePage gmailWindowMessagePage = gmailPage.clickComposeButton();
		gmailWindowMessagePage.typeAddressee(message.getAddressee())
				.typeSubject(message.getSubject())
				.typeContent(message.getContent())
				.send();
	}

	public void createNewDraftMessage(Message message) {
		GmailWindowMessagePage gmailWindowMessagePage = gmailPage.clickComposeButton();
		gmailWindowMessagePage.typeAddressee(message.getAddressee())
				.typeSubject(message.getSubject())
				.typeContent(message.getContent()).close();
	}

	public void sendLatestDraftMessage() {
		GmailDraftsPage gmailDraftsPage = gmailPage.openDrafts();
		if (gmailDraftsPage.isLatestDraftMessageWillBeOpenedInWindow()) {
			gmailDraftsPage.openLatestDraftMessageWindow().send();
		} else {
			gmailDraftsPage.openLatestDraftMessage().send();
		}
	}

	public Message getLatestDraftMessage() {
		String addressee, subject, content;
		GmailDraftsPage gmailDraftsPage = gmailPage.openDrafts();
		if (gmailDraftsPage.isLatestDraftMessageWillBeOpenedInWindow()) {
			GmailWindowMessagePage gmailWindowMessagePage = gmailDraftsPage.openLatestDraftMessageWindow();
			addressee = gmailWindowMessagePage.getAddressee();
			subject = gmailWindowMessagePage.getSubject();
			content = gmailWindowMessagePage.getContent();
		} else {
			GmailMessagePage gmailMessagePage = gmailDraftsPage.openLatestDraftMessage();
			addressee = gmailMessagePage.getAddressee();
			subject = gmailMessagePage.getSubject();
			content = gmailMessagePage.getContent();
		}
		return new Message(addressee, subject, content);
	}

	public Message getLatestSentMessage() {
		GmailMessagePage gmailMessagePage = gmailPage.openSentMail().openLatestSentMessage();
		String addressee = gmailMessagePage.getAddressee();
		String subject = gmailMessagePage.getSubject();
		String content = gmailMessagePage.getContent();
		return new Message(addressee, subject, content);
	}
}
