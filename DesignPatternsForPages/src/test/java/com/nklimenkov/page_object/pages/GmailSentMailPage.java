package test.java.com.nklimenkov.page_object.pages;

public class GmailSentMailPage extends GmailPage {

	public GmailMessagePage openLatestSentMessage() {
		openLatestMessage();
		return new GmailMessagePage();
	}
}
