package test.java.com.nklimenkov.browser;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.concurrent.TimeUnit;

public class Browser {

	private static WebDriver driver;
	private static final int DEFAULT_TIMEOUT = 10;

	public Browser() {
		driver = getWebDriverInstance();
	}

	public static WebDriver getWebDriverInstance() {
		if (driver == null) {
			initializeWebDriver();
		}
		return driver;
	}

	private static void initializeWebDriver() {
		driver = new ChromeWebDriver().getTargetDriver();
		driver.manage().timeouts().implicitlyWait(DEFAULT_TIMEOUT, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	public static void teardown() {
		if (driver != null) {
			driver.quit();
			driver = null;
		}
	}

	public void open(String URL) {
		driver = getWebDriverInstance();
		driver.get(URL);
	}

	public String getTitle() {
		return driver.getTitle();
	}

	public String getCurrentUrl() {
		return driver.getCurrentUrl();
	}

	public boolean isPresent(By locator) {
		return !driver.findElements(locator).isEmpty();
	}

	public boolean isDisplayed(By locator) {
		return driver.findElement(locator).isDisplayed();
	}

	public boolean isEnabled(By locator) {
		return driver.findElement(locator).isEnabled();
	}

	public void waitForElementToBePresent(By locator) {
		new WebDriverWait(driver, DEFAULT_TIMEOUT).until(ExpectedConditions.presenceOfElementLocated(locator));
	}

	public void waitForElementToBeClickable(By locator) {
		new WebDriverWait(driver, DEFAULT_TIMEOUT).until(ExpectedConditions.elementToBeClickable(locator));
	}

	public WebElementDecorator getElement(By locator) {
		return new WebElementDecorator(driver.findElement(locator), driver);
	}

	public void click(By locator) {
		getElement(locator).click();
	}

	public void sendKeys(By locator, CharSequence keysToSend) {
		getElement(locator).sendKeys(keysToSend);
	}

	public String getText(By locator) {
		return getElement(locator).getText();
	}

	public String getAttribute(By locator, String attribute) {
		return getElement(locator).getAttribute(attribute);
	}
}
