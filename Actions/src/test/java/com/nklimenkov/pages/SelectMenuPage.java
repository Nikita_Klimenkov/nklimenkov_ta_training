package test.java.com.nklimenkov.pages;

import org.openqa.selenium.By;

public class SelectMenuPage extends MainPage {

	private static final By SPEED_BUTTON_LOCATOR = By.id("speed-button");
	private static final String SPEED_MENU_ITEM_XPATH = "//ul[@id='speed-menu']/li[%s]";
	private static final By FILE_BUTTON_LOCATOR = By.id("files-button");
	private static final String FILE_MENU_ITEM_XPATH = "//ul[@id='files-menu']//div[@id='ui-id-%s']";
	private static final By NUMBER_BUTTON_LOCATOR = By.id("number-button");
	private static final String NUMBER_MENU_ITEM_XPATH = "//ul[@id='number-menu']/li[%s]/div";
	private static final By TITLE_BUTTON_LOCATOR = By.id("salutation-button");
	private static final By TITLE_BUTTON_SPAN_LOCATOR = By
			.xpath("//span[@id='salutation-button']/span[@class='ui-selectmenu-text']");
	private static final String TITLE_MENU_ITEM_XPATH = "//ul[@id='salutation-menu']/li[%s]/div";

	public SelectMenuPage selectSpeed(int i) {
		browser.click(SPEED_BUTTON_LOCATOR);
		browser.click(By.xpath(String.format(SPEED_MENU_ITEM_XPATH, i)));
		return this;
	}

	public SelectMenuPage selectFile(int i) {
		browser.click(FILE_BUTTON_LOCATOR);
		browser.click(By.xpath(String.format(FILE_MENU_ITEM_XPATH, i)));
		return this;
	}

	public SelectMenuPage selectNumber(int number) {
		browser.click(NUMBER_BUTTON_LOCATOR);
		for (int i = 1; i <= 19; i++) {
			By numberMenuItemLocator = By.xpath(String.format(NUMBER_MENU_ITEM_XPATH, i));
			if (Integer.parseInt(browser.getText(numberMenuItemLocator)) == number) {
				browser.click(numberMenuItemLocator);
				return this;
			}
		}
		return this;
	}

	public SelectMenuPage selectTitle(String title) {
		browser.click(TITLE_BUTTON_LOCATOR);
		for (int i = 2; i <= 6; i++) {
			By titleMenuItemLocator = By.xpath(String.format(TITLE_MENU_ITEM_XPATH, i));
			if (browser.getText(titleMenuItemLocator).equals(title)) {
				browser.click(titleMenuItemLocator);
				return this;
			}
		}
		return this;
	}

	public int getSelectedSpeed() {
		String attributeValue = browser.getAttribute(SPEED_BUTTON_LOCATOR, "aria-activedescendant");
		attributeValue = attributeValue.substring(attributeValue.length() - 1);
		return Integer.parseInt(attributeValue);
	}

	public int getSelectedFile() {
		String attributeValue = browser.getAttribute(FILE_BUTTON_LOCATOR, "aria-activedescendant");
		attributeValue = attributeValue.substring(attributeValue.length() - 1);
		return Integer.parseInt(attributeValue);
	}

	public int getSelectedNumber() {
		String attributeValue = browser.getAttribute(NUMBER_BUTTON_LOCATOR, "aria-activedescendant");
		attributeValue = attributeValue.substring(attributeValue.length() - 2);
		if (attributeValue.startsWith("-")) {
			attributeValue = attributeValue.substring(1);
		}
		return Integer.parseInt(attributeValue);
	}

	public String getSelectedTitle() {
		return browser.getText(TITLE_BUTTON_SPAN_LOCATOR);
	}
}
