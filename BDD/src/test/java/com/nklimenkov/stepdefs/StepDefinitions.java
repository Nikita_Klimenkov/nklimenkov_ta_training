package test.java.com.nklimenkov.stepdefs;

import org.testng.Assert;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import test.java.com.nklimenkov.service.EbayService;

public class StepDefinitions {

	EbayService ebayService = new EbayService();

	@Given("^I am on the www.ebay.com page$")
	public void i_am_on_the_ebay_dot_com_page() {
		ebayService.openEbayMainPage();
	}

	@Given("^I switch to English language$")
	public void i_switch_to_English_language() {
		ebayService.switchLanguageToEnglish();
	}

	@When("^I search for \"([^\"]*)\"$")
	public void i_search_for(String searchQuery) {
		ebayService.searchFor(searchQuery);
	}

	@When("^I open page of search result product number \"(\\d)\"$")
	public void i_open_page_of_search_result_product_number(int num) {
		ebayService.openPageOfSearchResultProductNumber(num);
	}

	@When("^I add opened product to shopping cart$")
	public void i_add_opened_product_to_shopping_cart() {
		ebayService.addOpenedProductToShoppingCart();
	}

	@Then("^Search result number \"(\\d+)\" should case insensitive contain \"([^\"]*)\"$")
	public void search_result_number_should_case_insensitive_contain(int num, String query) {
		String productName = ebayService.getNameOfSearchResultProductNumber(num);
		Assert.assertTrue(productName.toLowerCase().contains(query.toLowerCase()));
	}

	@Then("^Latest product in shopping cart should case insensitive contain \"([^\"]*)\"$")
	public void latest_product_in_shopping_cart_should_case_insensitive_contain(String query) {
		String shoppingCartLatestProductName = ebayService.getNameOfShoppingCartProductNumber(1);
		Assert.assertTrue(shoppingCartLatestProductName.toLowerCase().contains(query.toLowerCase()));
	}
}
