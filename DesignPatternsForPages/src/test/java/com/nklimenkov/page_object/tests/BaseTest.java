package test.java.com.nklimenkov.page_object.tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import test.java.com.nklimenkov.page_object.service.GmailService;
import test.java.com.nklimenkov.page_object.browser.Browser;

public class BaseTest {

	protected GmailService gmailService;

	@BeforeMethod
	public void setUp() {
		gmailService = new GmailService();
	}

	@AfterMethod
	public void cleanUp() {
		Browser.teardown();
	}
}
