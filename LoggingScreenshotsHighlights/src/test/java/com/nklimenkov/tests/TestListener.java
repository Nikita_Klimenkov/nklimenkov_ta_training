package test.java.com.nklimenkov.tests;

import org.apache.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import test.java.com.nklimenkov.browser.Browser;

public class TestListener implements ITestListener {

	private static Logger logger = Logger.getLogger(TestListener.class.getName());
	private static Browser browser = new Browser();

	@Override
	public void onTestStart(ITestResult iTestResult) {

	}

	@Override
	public void onTestSuccess(ITestResult iTestResult) {
		logger.info("Test " + iTestResult.getMethod().getMethodName() + " passed");
	}

	@Override
	public void onTestSkipped(ITestResult iTestResult) {
		logger.warn("Test " + iTestResult.getMethod().getMethodName() + " skipped");
	}

	@Override
	public void onTestFailure(ITestResult iTestResult) {
		logger.error("Test " + iTestResult.getMethod().getMethodName() + " failed");
		browser.takeScreenshot("Failed_test_" + iTestResult.getMethod().getMethodName());
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

	}

	@Override
	public void onStart(ITestContext iTestContext) {

	}

	@Override
	public void onFinish(ITestContext iTestContext) {

	}
}
