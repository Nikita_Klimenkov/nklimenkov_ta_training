package test.java.com.nklimenkov.tests;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import test.java.com.nklimenkov.pages.CheckboxRadioPage;

public class CheckboxRadioPageTest extends BaseTest {

	private CheckboxRadioPage checkboxRadioPage;

	@BeforeMethod
	public void setUpResizablePageTest() {
		checkboxRadioPage = mainPage.openCheckboxRadioPage();
		checkboxRadioPage.switchToDemoFrame();
	}

	@Test
	public void testRadioButtons() {
		for (int i = 1; i <= 3; i++) {
			checkboxRadioPage.selectRadioButton(i);
			Assert.assertEquals(checkboxRadioPage.getSelectedRadioButton(), i);
		}
	}

	@Test
	public void testCheckboxes() {
		int[] expectedToBeSelectedCheckboxes = new int[] { 1 };
		checkboxRadioPage.selectCheckboxes(expectedToBeSelectedCheckboxes);
		Assert.assertEquals(checkboxRadioPage.getSelectedCheckboxes(), expectedToBeSelectedCheckboxes);

		checkboxRadioPage.deselectAllCheckboxes();
		expectedToBeSelectedCheckboxes = new int[] { 3, 4 };
		checkboxRadioPage.selectCheckboxes(expectedToBeSelectedCheckboxes);
		Assert.assertEquals(checkboxRadioPage.getSelectedCheckboxes(), expectedToBeSelectedCheckboxes);

		checkboxRadioPage.deselectAllCheckboxes();
		expectedToBeSelectedCheckboxes = new int[] { 4, 2, 1 };
		checkboxRadioPage.selectCheckboxes(expectedToBeSelectedCheckboxes);
		Assert.assertEquals(checkboxRadioPage.getSelectedCheckboxes(), new int[] { 1, 2, 4 });

		checkboxRadioPage.deselectAllCheckboxes();
		expectedToBeSelectedCheckboxes = new int[] { 2, 4, 1, 3 };
		checkboxRadioPage.selectCheckboxes(expectedToBeSelectedCheckboxes);
		Assert.assertEquals(checkboxRadioPage.getSelectedCheckboxes(), new int[] { 1, 2, 3, 4 });
	}

	@Test
	public void testCheckboxesNestedInLabels() {
		int[] expectedToBeSelectedCheckboxes = new int[] { 1 };
		checkboxRadioPage.selectCheckboxesNestedInLabels(expectedToBeSelectedCheckboxes);
		Assert.assertEquals(checkboxRadioPage.getSelectedCheckboxesNestedInLabels(), expectedToBeSelectedCheckboxes);

		checkboxRadioPage.deselectAllCheckboxesNestedInLabels();
		expectedToBeSelectedCheckboxes = new int[] { 3, 4 };
		checkboxRadioPage.selectCheckboxesNestedInLabels(expectedToBeSelectedCheckboxes);
		Assert.assertEquals(checkboxRadioPage.getSelectedCheckboxesNestedInLabels(), expectedToBeSelectedCheckboxes);

		checkboxRadioPage.deselectAllCheckboxesNestedInLabels();
		expectedToBeSelectedCheckboxes = new int[] { 4, 2, 1 };
		checkboxRadioPage.selectCheckboxesNestedInLabels(expectedToBeSelectedCheckboxes);
		Assert.assertEquals(checkboxRadioPage.getSelectedCheckboxesNestedInLabels(), new int[] { 1, 2, 4 });

		checkboxRadioPage.deselectAllCheckboxesNestedInLabels();
		expectedToBeSelectedCheckboxes = new int[] { 2, 4, 1, 3 };
		checkboxRadioPage.selectCheckboxesNestedInLabels(expectedToBeSelectedCheckboxes);
		Assert.assertEquals(checkboxRadioPage.getSelectedCheckboxesNestedInLabels(), new int[] { 1, 2, 3, 4 });
	}
}
