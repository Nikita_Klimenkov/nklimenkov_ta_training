package test.java.com.nklimenkov.pages;

import java.util.ArrayList;
import org.openqa.selenium.By;

public class SelectablePage extends MainPage {

	private static final String ITEM_XPATH = "//ol[@id='selectable']/li[%s]";

	public SelectablePage selectItem(int item) {
		browser.click(By.xpath(String.format(ITEM_XPATH, item)));
		return this;
	}

	public SelectablePage selectItems(int[] itemsToSelect) {
		browser.CtrlKeyDown();
		for (int i = 0; i < itemsToSelect.length; i++) {
			browser.click(By.xpath(String.format(ITEM_XPATH, itemsToSelect[i])));
		}
		browser.CtrlKeyUp();
		return this;
	}

	public SelectablePage selectItemsFromTo(int firstItem, int lastItem) {
		By fromItem = By.xpath(String.format(ITEM_XPATH, firstItem));
		By toItem = By.xpath(String.format(ITEM_XPATH, lastItem));
		browser.dragAndDrop(fromItem, toItem);
		return this;
	}

	public int[] getSelectedItems() {
		ArrayList<Integer> selectedItemsList = new ArrayList<Integer>();
		for (int i = 1; i <= 7; i++) {
			By itemLocator = By.xpath(String.format(ITEM_XPATH, i));
			String classValue = browser.getAttribute(itemLocator, "class");
			if (classValue.contains("selected")) {
				selectedItemsList.add(i);
			}
		}
		int[] selectedItems = new int[selectedItemsList.size()];
		for (int i = 0; i < selectedItems.length; i++) {
			selectedItems[i] = selectedItemsList.get(i).intValue();
		}
		return selectedItems;
	}
}
