package test.java.com.nklimenkov.pages;

import org.openqa.selenium.By;

public class SearchResultsPage extends MainPage {

	private static final String SEARCH_RESULT_ITEM = "//div[@id='ResultSetItems']/ul/li[%s]/h3/a";

	public ProductPage openProductPageNumber(int i) {
		if (browser.isPresent(By.xpath(String.format(SEARCH_RESULT_ITEM, i)))) {
			browser.click(By.xpath(String.format(SEARCH_RESULT_ITEM, i)));
			return new ProductPage();
		} else {
			return null;
		}
	}

	public String getNameOfProductNumber(int i) {
		if (browser.isPresent(By.xpath(String.format(SEARCH_RESULT_ITEM, i)))) {
			return browser.getText(By.xpath(String.format(SEARCH_RESULT_ITEM, i)));
		} else {
			return "null";
		}
	}
}
