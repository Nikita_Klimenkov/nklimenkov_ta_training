package test.java.com.nklimenkov.page_object.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import test.java.com.nklimenkov.bo.Account;
import test.java.com.nklimenkov.bo.Message;

public class DraftTest extends BaseTest {
	@Test
	public void testCreateNewDraftMessage() {
		gmailService.signIn(Account.getDefaultAccount());
		gmailService.createNewDraftMessage(Message.getDefaultMessage());
		Message draftMessage = gmailService.getLatestDraftMessage();
		Assert.assertTrue(draftMessage.equals(Message.getDefaultMessage()));
		gmailService.signOut();
	}
}
