package test.java.com.nklimenkov.page_factory.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GmailMessagePage extends GmailPage {

	@FindBy(xpath = "//span[@class='g2']")
	private WebElement addresseeSpan;

	@FindBy(xpath = "//h2[@class='hP']")
	private WebElement subjectHeader;

	@FindBy(xpath = "//div[@class='Am aO9 Al editable LW-avf']")
	private WebElement contentDivWithConversationViewOn;

	@FindBy(xpath = "//div[@class='ii gt ']/div/div[@dir='ltr']")
	private WebElement contentDivWithConversationViewOff;

	@FindBy(xpath = "//div[@class='T-I J-J5-Ji aoO T-I-atl L3']")
	private WebElement sendButton;

	public String getAddressee() {
		return browser.getAttribute(addresseeSpan, "email");
	}

	public String getSubject() {
		return browser.getText(subjectHeader);
	}

	public String getContent() {
		if (browser.isPresentAndDisplayed(contentDivWithConversationViewOn)) {
			return browser.getText(contentDivWithConversationViewOn);
		} else {
			return browser.getText(contentDivWithConversationViewOff);
		}
	}

	public GmailMessagePage send() {
		browser.click(sendButton);
		return this;
	}
}
