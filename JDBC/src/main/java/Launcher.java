package main.java;

public class Launcher {

    public static void main(String[] args) {
        DatabaseConfigurations.setUp();
        StaffTableUI.startUserDialogue();
        DatabaseConfigurations.teardown();
    }
}
