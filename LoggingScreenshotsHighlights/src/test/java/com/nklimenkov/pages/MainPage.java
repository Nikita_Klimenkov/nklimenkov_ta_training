package test.java.com.nklimenkov.pages;

import org.openqa.selenium.By;

public class MainPage extends BasePage {

	private static final String URL = "https://www.ebay.com";

	private static final By SEARCH_TEXTBOX_LOCATOR = By.id("gh-ac");
	private static final By SEARCH_BUTTON_LOCATOR = By.id("gh-btn");
	private static final By CURRENT_LANGUAGE_LOCATOR = By
			.xpath("//a[@id='gh-eb-Geo-a-default']/span[@class='gh-eb-Geo-txt']");
	private static final By ENGLISH_LANGUAGE_LOCATOR = By
			.xpath("//a[@id='gh-eb-Geo-a-en']/span[@class='gh-eb-Geo-txt']");

	public MainPage open() {
		browser.open(URL);
		return this;
	}

	public String getCurrentLanguage() {
		return browser.getText(CURRENT_LANGUAGE_LOCATOR);
	}

	public MainPage clickCurrentLanguage() {
		browser.click(CURRENT_LANGUAGE_LOCATOR);
		return this;
	}

	public MainPage clickEnglishLanguage() {
		browser.click(ENGLISH_LANGUAGE_LOCATOR);
		return this;
	}

	public MainPage clickSearchTextbox() {
		browser.click(SEARCH_TEXTBOX_LOCATOR);
		return this;
	}

	public MainPage clearSearchTextbox() {
		browser.clear(SEARCH_TEXTBOX_LOCATOR);
		return this;
	}

	public MainPage typeToSearchTextbox(String text) {
		clearSearchTextbox();
		browser.sendKeys(SEARCH_TEXTBOX_LOCATOR, text);
		return this;
	}

	public SearchResultsPage clickSearchButton() {
		browser.click(SEARCH_BUTTON_LOCATOR);
		return new SearchResultsPage();
	}

	public MainPage highlightCurrentLanguage() {
		browser.highlightElement(CURRENT_LANGUAGE_LOCATOR, 3, "red");
		return this;
	}

	public MainPage unHighlightCurrentLanguage() {
		browser.unHighlightElement(CURRENT_LANGUAGE_LOCATOR);
		return this;
	}
}
