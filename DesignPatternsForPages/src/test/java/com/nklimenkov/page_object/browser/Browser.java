package test.java.com.nklimenkov.page_object.browser;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.concurrent.TimeUnit;

public class Browser {

	private static WebDriver driver;
	private static final int DEFAULT_TIMEOUT = 10;

	public Browser() {
		driver = getWebDriverInstance();
	}

	public static WebDriver getWebDriverInstance() {
		if (driver == null) {
			initializeWebDriver();
		}
		return driver;
	}

	private static void initializeWebDriver() {
		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(DEFAULT_TIMEOUT, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	public static void teardown() {
		if (driver != null) {
			driver.quit();
			driver = null;
		}
	}

	public void open(String URL) {
		driver.get(URL);
	}

	public String getTitle() {
		return driver.getTitle();
	}

	public String getCurrentUrl() {
		return driver.getCurrentUrl();
	}

	public boolean isPresent(By locator) {
		return !driver.findElements(locator).isEmpty();
	}

	public boolean isDisplayed(By locator) {
		return driver.findElement(locator).isDisplayed();
	}

	public boolean isEnabled(By locator) {
		return driver.findElement(locator).isEnabled();
	}

	public void wait(By locator) {
		new WebDriverWait(driver, DEFAULT_TIMEOUT).until(ExpectedConditions.elementToBeClickable(locator));
	}

	public void click(By locator) {
		wait(locator);
		driver.findElement(locator).click();
	}

	public void sendKeys(By locator, CharSequence keysToSend) {
		wait(locator);
		driver.findElement(locator).sendKeys(keysToSend);
	}

	public String getText(By locator) {
		wait(locator);
		return driver.findElement(locator).getText();
	}

	public String getAttribute(By locator, String attribute) {
		return driver.findElement(locator).getAttribute(attribute);
	}
}
