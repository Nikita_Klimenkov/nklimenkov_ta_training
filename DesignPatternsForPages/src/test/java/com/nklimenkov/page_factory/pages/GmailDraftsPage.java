package test.java.com.nklimenkov.page_factory.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GmailDraftsPage extends GmailPage {

	@FindBy(xpath = "//div[@role='main']//tr[1]//div[@class='yW']/font")
	private WebElement typeOfLatestMessage;

	public boolean latestDraftMessageWillBeOpenedInWindow() {
		return browser.getText(typeOfLatestMessage).equals("Draft");
	}

	public GmailMessagePage openLatestDraftMessage() {
		openLatestMessage();
		return new GmailMessagePage();
	}

	public GmailWindowMessagePage openLatestDraftMessageWindow() {
		openLatestMessage();
		return new GmailWindowMessagePage();
	}
}
