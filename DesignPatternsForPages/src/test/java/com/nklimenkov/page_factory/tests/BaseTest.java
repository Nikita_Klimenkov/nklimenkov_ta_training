package test.java.com.nklimenkov.page_factory.tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import test.java.com.nklimenkov.page_factory.browser.Browser;
import test.java.com.nklimenkov.page_factory.service.GmailService;

public class BaseTest {

	protected GmailService gmailService;

	@BeforeMethod
	public void setUp() {
		gmailService = new GmailService();
	}

	@AfterMethod
	public void cleanUp() {
		Browser.teardown();
	}
}
