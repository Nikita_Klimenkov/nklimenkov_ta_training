package test.java.com.nklimenkov.service;

import test.java.com.nklimenkov.pages.ProductPage;
import test.java.com.nklimenkov.pages.MainPage;
import test.java.com.nklimenkov.pages.SearchResultsPage;
import test.java.com.nklimenkov.pages.ShoppingCartPage;

public class EbayService {

	private MainPage mainPage = new MainPage();
	private SearchResultsPage searchResultsPage = new SearchResultsPage();
	private ProductPage productPage = new ProductPage();
	private ShoppingCartPage shoppingCartPage = new ShoppingCartPage();

	public void openEbayMainPage() {
		mainPage.open();
	}

	public void switchLanguageToEnglish() {
		if (!mainPage.getCurrentLanguage().equals("English")) {
			mainPage.clickCurrentLanguage().clickEnglishLanguage().clickSearchTextbox();
			// clickSearchTextbox() here is to make language settings pop-up message
			// disappear as it covers Search button
		}
	}

	public void highlightCurrentLanguage() {
		mainPage.highlightCurrentLanguage();
	}

	public void unHighlightCurrentLanguage() {
		mainPage.unHighlightCurrentLanguage();
	}

	public void searchFor(String searchQuery) {
		mainPage.typeToSearchTextbox(searchQuery).clickSearchButton();
	}

	public void highlightNameOfSearchResultProductNumber(int num) {
		searchResultsPage.highlightNameOfProductNumber(num);
	}

	public void unHighlightNameOfSearchResultProductNumber(int num) {
		searchResultsPage.unHighlightNameOfProductNumber(num);
	}

	public String getNameOfSearchResultProductNumber(int num) {
		return searchResultsPage.getNameOfProductNumber(num);
	}

	public void openPageOfSearchResultProductNumber(int num) {
		searchResultsPage.openProductPageNumber(num);
	}

	public void addOpenedProductToShoppingCart() {
		productPage.addProductToShoppingCart();
	}

	public void highlightNameOfShoppingCartProductNumber(int num) {
		shoppingCartPage.highlightNameOfProductNumber(num);
	}

	public void unHighlightNameOfShoppingCartProductNumber(int num) {
		shoppingCartPage.unHighlightNameOfProductNumber(num);
	}

	public String getNameOfShoppingCartProductNumber(int num) {
		return shoppingCartPage.getNameOfProductNumber(num);
	}
}
